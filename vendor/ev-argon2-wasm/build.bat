REM context message
echo "running build with bat script"

REM Clean and prepare context
rmdir /s /q dist wasm
mkdir wasm

REM Lint before building the static TS part
call npm run eslint || exit /b 1

REM Create the build image and build the C code to wasm
docker build -t ev-argon2-wasm-build .
docker run -v %CD%/wasm:/out ev-argon2-wasm-build

REM Build the node module
call npm run build || exit /b 1