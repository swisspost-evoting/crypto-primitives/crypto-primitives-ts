# ev-argon2-wasm

This module had been copied from the great work made on [this repository](https://github.com/Daninet/hash-wasm).

We removed all the unused algorithms and adapted the code to build it on our environment.

## Build

There are 2 build scripts, depending on which platform you are.

To run it locally on your dev environment, you can either:
- run _build.sh_ with a git bash.
- run _build.bat_ with a windows cmd.

Maven will use the right script according to the platform you are on.

Because the sh script is also used by our internal jenkins job, the location of the docker volume has to be customizable. 

