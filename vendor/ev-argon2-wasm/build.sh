# Fail script on any command failure
set -e

# context message
echo "running build with sh script"

# Get docker volume location if run on jenkins
DOCKER_VOLUME="${DOCKER_VOLUME:-$PWD}"

# Clean and prepare context
rm -rf dist wasm
mkdir -p wasm

# Lint before build the static TS part
npm run eslint

# Create the build image and build the C code to wasm
docker build \
    --build-arg="http_proxy=${HTTP_PROXY}" \
    --build-arg="https_proxy=${HTTP_PROXY}" \
    --build-arg="no_proxy=${NO_PROXY}" \
    -t ev-argon2-wasm-build .
MSYS_NO_PATHCONV=1 docker run \
    -v $DOCKER_VOLUME/wasm:/out \
    ev-argon2-wasm-build

# Build the node module
npm run build
