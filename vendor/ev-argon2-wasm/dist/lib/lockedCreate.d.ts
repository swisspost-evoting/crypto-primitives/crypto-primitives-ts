import Mutex from './mutex';
import { IWASMInterface } from './WASMInterface';
export default function lockedCreate(mutex: Mutex, binary: any, hashLength: number): Promise<IWASMInterface>;
