/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ImmutableUint8Array} from "../src/immutable_uint8Array";
import {NullPointerError} from "../src/error/null_pointer_error";
import {IllegalArgumentError} from "../src/error/illegal_argument_error";

test("from with null array throws NullPointerError", () => {
	expect(() => ImmutableUint8Array.from(null)).toThrow(NullPointerError);
});

test("from with array of null throws IllegalArgumentError", () => {
	expect(() => ImmutableUint8Array.from([null])).toThrow(IllegalArgumentError);
});

test("modifying wrapped array does not modify immutable array", () => {
	const uint8Array = Uint8Array.of(1, 2, 3, 4);
	const immutableUint8Array = ImmutableUint8Array.from(uint8Array);

	uint8Array.fill(0);

	expect(uint8Array[0]).toBe(0);
	expect(immutableUint8Array.get(0)).toBe(1);
	expect(immutableUint8Array.get(1)).toBe(2);
	expect(immutableUint8Array.get(2)).toBe(3);
	expect(immutableUint8Array.get(3)).toBe(4);
	expect(immutableUint8Array.length).toBe(4);
});

test("value returns copy", () => {
	const uint8Array = Uint8Array.of(1, 2, 3, 4);
	const immutableUint8Array = ImmutableUint8Array.from(uint8Array);

	const uint8ArrayCopy = immutableUint8Array.value();
	uint8ArrayCopy.fill(0);

	expect(uint8ArrayCopy).not.toEqual<Uint8Array>(uint8Array);
	expect(immutableUint8Array.get(0)).toBe(1);
	expect(immutableUint8Array.get(1)).toBe(2);
	expect(immutableUint8Array.get(2)).toBe(3);
	expect(immutableUint8Array.get(3)).toBe(4);
});

test("toArray returns copy", () => {
	const uint8Array = Uint8Array.of(1, 2, 3, 4);
	const immutableUint8Array = ImmutableUint8Array.from(uint8Array);

	const array = immutableUint8Array.toArray();
	array.fill(0);

	expect(uint8Array[0]).toBe(1);
	expect(uint8Array[1]).toBe(2);
	expect(uint8Array[2]).toBe(3);
	expect(uint8Array[3]).toBe(4);
	expect(immutableUint8Array.get(0)).toBe(1);
	expect(immutableUint8Array.get(1)).toBe(2);
	expect(immutableUint8Array.get(2)).toBe(3);
	expect(immutableUint8Array.get(3)).toBe(4);
});