/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GroupTestData} from "../tools/data/group_test_data";
import {ZqGroupGenerator} from "../tools/generators/zq_group_generator";
import {ZqGroup} from "../../src/math/zq_group";
import {ZqElement} from "../../src/math/zq_element";
import {ExponentiationProof} from "../../src/zeroknowledgeproofs/exponentiation_proof";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";

const group = GroupTestData.getGroup();
const otherGroup = GroupTestData.getDifferentGqGroup(group);
const zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(group));
const otherZqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(otherGroup));

let e: ZqElement;
let z: ZqElement;

beforeEach(() => {
	e = zqGroupGenerator.genRandomZqElementMember();
	z = zqGroupGenerator.genRandomZqElementMember();
});

describe("An ExponentiationProof with", () => {

	test("any null parameters throws NullPointerError", () => {
		expect(() => new ExponentiationProof(e, null)).toThrow(NullPointerError);
		expect(() => new ExponentiationProof(null, z)).toThrow(NullPointerError);
	});

	test("different group throws IllegalArgumentError", () => {
		const otherZ = otherZqGroupGenerator.genRandomZqElementMember();
		expect(() => new ExponentiationProof(e, otherZ)).toThrow(new IllegalArgumentError("e and z must be from the same group."));
	});

	test("valid arguments does not throw", () => {
		expect(() => new ExponentiationProof(e, z)).not.toThrow();
	});

});