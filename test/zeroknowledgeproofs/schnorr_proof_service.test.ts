/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ZqElement} from "../../src/math/zq_element";
import {GqGroup} from "../../src/math/gq_group";
import {GqElement} from "../../src/math/gq_element";
import {RandomService} from "../../src/math/random_service";
import {GroupTestData} from "../tools/data/group_test_data";
import {ZqGroup} from "../../src/math/zq_group";
import {SchnorrProofService} from "../../src/zeroknowledgeproofs/schnorr_proof_service";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {TestHashService} from "../hashing/test_hash_service";
import {HashService} from "../../src/hashing/hash_service";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";
import realValuesJson from './verify-schnorr.json';
import {GqGroupGenerator} from "../tools/gq_group_generator";
import {ZqGroupGenerator} from "../tools/generators/zq_group_generator";
import {SchnorrProof} from "../../src/zeroknowledgeproofs/schnorr_proof";

const group = GroupTestData.getGroup();
const randomService = new RandomService();
const hashService = TestHashService.create(group.q);
const schnorrProofService = new SchnorrProofService(randomService, hashService);
const gqGroupGenerator = new GqGroupGenerator(group);
const zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(group));

describe("SchnorrProofService calling", () => {

	test("constructor with any null parameters throws NullPointerError", () => {
		expect(() => new SchnorrProofService(null, hashService)).toThrow(NullPointerError);
		expect(() => new SchnorrProofService(randomService, null)).toThrow(NullPointerError);
	});

	describe("computePhiSchnorr with", () => {

		let base: GqElement;
		let exponent: ZqElement;

		beforeEach(() => {
			base = gqGroupGenerator.genMember();
			exponent = zqGroupGenerator.genRandomZqElementMember();
		});

		test("valid parameters does not throw", () => {
			expect(() => SchnorrProofService.computePhiSchnorr(exponent, base)).not.toThrow();
		});

		test("null parameters throws NullPointerError", () => {
			expect(() => SchnorrProofService.computePhiSchnorr(null, base)).toThrow(NullPointerError);
			expect(() => SchnorrProofService.computePhiSchnorr(exponent, null)).toThrow(NullPointerError);
		});

		test("specific values gives expected image", () => {
			const testValues = new TestValues();

			expect(testValues.expectedOneImage.equals(testValues.resultZeroOne)).toBe(true);
			expect(testValues.expectedOneImage.equals(testValues.resultZeroFour)).toBe(true);
			expect(testValues.expectedOneImage.equals(testValues.resultZeroFive)).toBe(true);
			expect(testValues.expectedOneImage.equals(testValues.resultZeroNine)).toBe(true);

			expect(testValues.expectedOneImage.equals(testValues.resultThreeOne)).toBe(true);
			expect(testValues.expectedFiveImage.equals(testValues.resultThreeFour)).toBe(true);
			expect(testValues.expectedSevenImage.equals(testValues.resultThreeFive)).toBe(true);
			expect(testValues.expectedTwentyOneImage.equals(testValues.resultThreeNine)).toBe(true);
		});
	});

	describe("genSchnorrProof with", () => {

		let witness: ZqElement;
		let statement: GqElement;
		let auxiliaryInformation: string[];

		beforeEach(() => {
			const testValues = new TestValues();
			witness = new ZqGroupGenerator(testValues.zqGroup).genRandomZqElementMember();
			statement = new GqGroupGenerator(testValues.gqGroup).genMember().group.generator.exponentiate(witness);
			auxiliaryInformation = ["asdfsdfglkm", "sdfsd0sdfksédf"];
		});

		test("valid parameters does not throw", () => {
			expect(() => schnorrProofService.genSchnorrProof(witness, statement, auxiliaryInformation)).not.toThrow();
		});

		test("with any null parameters throws NullPointerError", () => {
			expect(() => schnorrProofService.genSchnorrProof(null, statement, auxiliaryInformation))
				.toThrow(NullPointerError);
			expect(() => schnorrProofService.genSchnorrProof(witness, null, auxiliaryInformation))
				.toThrow(NullPointerError);
			expect(() => schnorrProofService.genSchnorrProof(witness, statement, null)).toThrow(NullPointerError);
		});

		test("aux information containing null throws IllegalArgumentError", () => {
			const auxiliaryInformationWithNull = ["test", null];
			expect(() => schnorrProofService.genSchnorrProof(witness, statement, auxiliaryInformationWithNull))
				.toThrow(new IllegalArgumentError("The auxiliary information must not contain null objects."));
		});

		test("statement and witness having different group orders throws IllegalArgumentError", () => {
			const differentZqGroupGenerator = new ZqGroupGenerator(new ZqGroup(witness.group.q.add(ImmutableBigInteger.ONE)));
			const witnessDifferentGroup = differentZqGroupGenerator.genRandomZqElementMember();
			expect(() => schnorrProofService.genSchnorrProof(witnessDifferentGroup, statement, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The witness must have the same group order as the statement."));
		});

		test("", () => {
			const differentWitness = witness.add(ZqElement.create(ImmutableBigInteger.ONE, witness.group));
			expect(() => schnorrProofService.genSchnorrProof(differentWitness, statement, auxiliaryInformation)).toThrow();
		});

	});

	describe("verifyExponentiation with", () => {

		let schnorrProof: SchnorrProof;
		let witness: ZqElement;
		let statement: GqElement;
		const auxiliaryInformation = ["aux", "4"];

		beforeEach(() => {
			const testValues = new TestValues();
			witness = new ZqGroupGenerator(testValues.zqGroup).genRandomZqElementMember();
			statement = new GqGroupGenerator(testValues.gqGroup).genMember().group.generator.exponentiate(witness);
			schnorrProof = schnorrProofService.genSchnorrProof(witness, statement, auxiliaryInformation);
		});

		test("valid parameters returns true", () => {
			expect(schnorrProofService.verifySchnorrProof(schnorrProof, statement, auxiliaryInformation)).toBe(true);
		});

		test("empty auxiliary information returns true", () => {
			const schnorrProof: SchnorrProof = schnorrProofService.genSchnorrProof(witness, statement, auxiliaryInformation);
			expect(schnorrProofService.verifySchnorrProof(schnorrProof, statement, auxiliaryInformation)).toBe(true);
		});

		test("with any null parameters throws NullPointerError", () => {
			expect(() => schnorrProofService.verifySchnorrProof(null, statement, auxiliaryInformation))
				.toThrow(NullPointerError);
			expect(() => schnorrProofService.verifySchnorrProof(schnorrProof, null, auxiliaryInformation))
				.toThrow(NullPointerError);
			expect(() => schnorrProofService.verifySchnorrProof(schnorrProof, statement, null))
				.toThrow(NullPointerError);
		});

		test("aux information containing null throws IllegalArgumentError", () => {
			const auxiliaryInformationWithNull = ["test", null];
			expect(() => schnorrProofService.genSchnorrProof(witness, statement, auxiliaryInformationWithNull))
				.toThrow(new IllegalArgumentError("The auxiliary information must not contain null objects."));
		});

		describe('with real values gives expected result', () => {

			interface JsonFileArgument {
				schnorrProof: SchnorrProof;
				statement: GqElement,
				auxiliaryInformation: string[];
				result: boolean;
				description: string;
			}

			function jsonFileArgumentProvider(): JsonFileArgument[] {

				const parametersList: any[] = JSON.parse(JSON.stringify(realValuesJson));

				const args: JsonFileArgument[] = [];
				parametersList.forEach(testParameters => {

					// Context.
					const p: ImmutableBigInteger = readValue(testParameters.context.p);
					const q: ImmutableBigInteger = readValue(testParameters.context.q);
					const g: ImmutableBigInteger = readValue(testParameters.context.g);

					const gqGroup: GqGroup = new GqGroup(p, q, g);
					const zqGroup: ZqGroup = new ZqGroup(q);

					// Parse statement (statement) parameter
					const state: ImmutableBigInteger = readValue(testParameters.input.statement);
					const statement: GqElement = GqElement.fromValue(state, gqGroup);

					// Parse SchnorrProof (proof) parameters
					const proof: any = testParameters.input.proof;

					const e: ZqElement = ZqElement.create(readValue(proof.e), zqGroup);
					const z: ZqElement = ZqElement.create(readValue(proof.z), zqGroup);
					const schnorrProof: SchnorrProof = new SchnorrProof(e, z);

					// Parse auxiliary information parameters.
					const auxiliaryInformation: string[] = testParameters.input.additional_information;

					// Parse output parameters
					const result: boolean = testParameters.output.result;

					args.push({
						schnorrProof: schnorrProof,
						statement: statement,
						auxiliaryInformation: auxiliaryInformation,
						result: result,
						description: testParameters.description
					});
				});

				return args;
			}

			test.each(jsonFileArgumentProvider())("$description", ({
																	   schnorrProof,
																	   statement,
																	   auxiliaryInformation,
																	   result,
																	   // @ts-ignore description is used by test definition
																	   description
																   }) => {
				const schnorrProofService: SchnorrProofService = new SchnorrProofService(randomService, new HashService());

				const actual: boolean = schnorrProofService.verifySchnorrProof(schnorrProof, statement, auxiliaryInformation);

				expect(actual).toEqual(Boolean(result));
			});
		});

	});

});

function readValue(value: string): ImmutableBigInteger {
	return ImmutableBigInteger.fromString(value.substring(2), 16);
}

class TestValues {
	private readonly p = ImmutableBigInteger.fromNumber(59);
	private readonly q = ImmutableBigInteger.fromNumber(29);
	private readonly g = ImmutableBigInteger.fromNumber(3);

	public readonly zqGroup = new ZqGroup(this.q);
	public readonly gqGroup = new GqGroup(this.p, this.q, this.g);

	public readonly zZero = ZqElement.create(ImmutableBigInteger.fromNumber(0), this.zqGroup);
	public readonly gOne = GqElement.fromValue(ImmutableBigInteger.fromNumber(1), this.gqGroup);
	public readonly zThree = ZqElement.create(ImmutableBigInteger.fromNumber(3), this.zqGroup);
	public readonly gFour = GqElement.fromValue(ImmutableBigInteger.fromNumber(4), this.gqGroup);
	public readonly gFive = GqElement.fromValue(ImmutableBigInteger.fromNumber(5), this.gqGroup);
	public readonly gSeven = GqElement.fromValue(ImmutableBigInteger.fromNumber(7), this.gqGroup);
	public readonly gNine = GqElement.fromValue(ImmutableBigInteger.fromNumber(9), this.gqGroup);
	public readonly gTwentyOne = GqElement.fromValue(ImmutableBigInteger.fromNumber(21), this.gqGroup);

	public readonly resultZeroOne = SchnorrProofService.computePhiSchnorr(this.zZero, this.gOne);
	public readonly resultZeroFour = SchnorrProofService.computePhiSchnorr(this.zZero, this.gFour);
	public readonly resultZeroFive = SchnorrProofService.computePhiSchnorr(this.zZero, this.gFive);
	public readonly resultZeroNine = SchnorrProofService.computePhiSchnorr(this.zZero, this.gNine);
	public readonly resultThreeOne = SchnorrProofService.computePhiSchnorr(this.zThree, this.gOne);
	public readonly resultThreeFour = SchnorrProofService.computePhiSchnorr(this.zThree, this.gFour);
	public readonly resultThreeFive = SchnorrProofService.computePhiSchnorr(this.zThree, this.gFive);
	public readonly resultThreeNine = SchnorrProofService.computePhiSchnorr(this.zThree, this.gNine);

	public readonly expectedOneImage = this.gOne;
	public readonly expectedSevenImage = this.gSeven;
	public readonly expectedTwentyOneImage = this.gTwentyOne;
	public readonly expectedFiveImage = this.gFive;
}
