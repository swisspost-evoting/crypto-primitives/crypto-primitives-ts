/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {vectorAddition, vectorExponentiation, vectorMultiplication, vectorScalarMultiplication} from "../../src/zeroknowledgeproofs/vector_utils";
import {ZqGroup} from "../../src/math/zq_group";
import {GroupVector} from "../../src/group_vector";
import {ZqElement} from "../../src/math/zq_element";
import {GroupTestData} from "../tools/data/group_test_data";
import {ZqGroupGenerator} from "../tools/generators/zq_group_generator";
import {RandomService} from "../../src/math/random_service";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";
import {GqGroup} from "../../src/math/gq_group";
import {GqElement} from "../../src/math/gq_element";
import {GqGroupGenerator} from "../tools/gq_group_generator";

const group = GroupTestData.getGroup();
const otherGroup = GroupTestData.getDifferentGqGroup(group);
const zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(group));
const otherZqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(otherGroup));
const gqGroupGenerator = new GqGroupGenerator(group);
const otherGqGroupGenerator = new GqGroupGenerator(otherGroup);

const MAX_LENGTH = 10;
const randomService = new RandomService();

describe("vector addition with", () => {

	let vectorSize: number;
	let firstVector: GroupVector<ZqElement, ZqGroup>;
	let secondVector: GroupVector<ZqElement, ZqGroup>;

	beforeEach(() => {
		vectorSize = randomService.nextInt(MAX_LENGTH) + 1;
		firstVector = zqGroupGenerator.genRandomZqElementVector(vectorSize);
		secondVector = zqGroupGenerator.genRandomZqElementVector(vectorSize);
	});

	test("valid parameters gives valid vector", () => {
		const sum: GroupVector<ZqElement, ZqGroup> = vectorAddition(firstVector, secondVector);

		expect(sum.size).toEqual(vectorSize);
		expect(firstVector.group.equals(sum.group)).toBe(true);
	});

	test("any null parameter throws NullPointerException", () => {
		expect(() => vectorAddition(null, secondVector)).toThrow(NullPointerError);
		expect(() => vectorAddition(firstVector, null)).toThrow(NullPointerError);
	});

	test("vectors of different sizes throws IllegalArgumentException", () => {
		const tooLongVector = zqGroupGenerator.genRandomZqElementVector(vectorSize + 1);

		expect(() => vectorAddition(tooLongVector, secondVector))
			.toThrow(new IllegalArgumentError("The vectors to be added must have the same size."));
	});

	test("vectors of different groups throws IllegalArgumentException", () => {
		const diffGroupVector = otherZqGroupGenerator.genRandomZqElementVector(vectorSize);

		expect(() => vectorAddition(diffGroupVector, secondVector)).toThrow(new IllegalArgumentError("Both vectors must have the same group."));
	});

	describe("specific values gives expected vector addition", () => {
		interface Argument {
			first: GroupVector<ZqElement, ZqGroup>;
			second: GroupVector<ZqElement, ZqGroup>;
			expected: GroupVector<ZqElement, ZqGroup>;
		}

		function additionArgumentsProvider(): Argument[] {
			const q = ImmutableBigInteger.fromString("5");
			const zqGroup = new ZqGroup(q);

			const firstVector = GroupVector.of<ZqElement, ZqGroup>(ZqElement.create(1, zqGroup), ZqElement.create(4, zqGroup));
			const secondVector = GroupVector.of<ZqElement, ZqGroup>(ZqElement.create(3, zqGroup), ZqElement.create(2, zqGroup));
			const zeroVector = GroupVector.of<ZqElement, ZqGroup>(ZqElement.create(0, zqGroup), ZqElement.create(0, zqGroup));
			const firstVectorNegated = GroupVector.from<ZqElement, ZqGroup>(firstVector.elements.map(_ => (_.negate())));

			const expected = GroupVector.of<ZqElement, ZqGroup>(ZqElement.create(4, zqGroup), ZqElement.create(1, zqGroup));

			return [
				{first: firstVector, second: secondVector, expected: expected},
				{first: firstVector, second: zeroVector, expected: firstVector},
				{first: firstVector, second: firstVectorNegated, expected: zeroVector}
			];
		}

		test.each(additionArgumentsProvider())("addition", ({first, second, expected}) => {
			const addition = vectorAddition(first, second);

			expect(addition.equals(expected)).toBe(true);
		});
	});

});

describe("vector multiply with", () => {

	let vectorSize: number;
	let firstVector: GroupVector<GqElement, GqGroup>;
	let secondVector: GroupVector<GqElement, GqGroup>;

	beforeEach(() => {
		vectorSize = randomService.nextInt(MAX_LENGTH) + 1;
		firstVector = gqGroupGenerator.genRandomGqElementVector(vectorSize);
		secondVector = gqGroupGenerator.genRandomGqElementVector(vectorSize);
	});

	test("valid parameters gives valid vector", () => {
		const product: GroupVector<GqElement, GqGroup> = vectorMultiplication(firstVector, secondVector);

		expect(product.size).toEqual(vectorSize);
		expect(firstVector.group.equals(product.group)).toBe(true);
	});

	test("any null parameter throws NullPointerError", () => {
		expect(() => vectorMultiplication(null, secondVector)).toThrow(NullPointerError);
		expect(() => vectorMultiplication(firstVector, null)).toThrow(NullPointerError);
	});

	test("vectors of different sizes throws IllegalArgumentException", () => {
		const tooLongVector = gqGroupGenerator.genRandomGqElementVector(vectorSize + 1);

		expect(() => vectorMultiplication(tooLongVector, secondVector))
			.toThrow(new IllegalArgumentError("The vectors to be multiplied must have the same size."));
	});

	test("vectors of different groups throws IllegalArgumentException", () => {
		const diffGroupVector = otherGqGroupGenerator.genRandomGqElementVector(vectorSize);

		expect(() => vectorMultiplication(diffGroupVector, secondVector)).toThrow(new IllegalArgumentError("Both vectors must have the same group."));
	});

	describe("specific values gives expected vector multiplication", () => {
		interface Argument {
			first: GroupVector<GqElement, GqGroup>;
			second: GroupVector<GqElement, GqGroup>;
			expected: GroupVector<GqElement, GqGroup>;
		}

		function multiplicationArgumentsProvider(): Argument[] {
			const p = ImmutableBigInteger.fromString("11");
			const q = ImmutableBigInteger.fromString("5");
			const g = ImmutableBigInteger.fromString("3");
			const gqGroup = new GqGroup(p, q, g);

			const firstVector = GroupVector.of<GqElement, GqGroup>(GqElement.fromValue(ImmutableBigInteger.fromNumber(1), gqGroup), GqElement.fromValue(ImmutableBigInteger.fromNumber(4), gqGroup));
			const secondVector = GroupVector.of<GqElement, GqGroup>(GqElement.fromValue(ImmutableBigInteger.fromNumber(3), gqGroup), GqElement.fromValue(ImmutableBigInteger.fromNumber(4), gqGroup));
			const oneVector = GroupVector.of<GqElement, GqGroup>(GqElement.fromValue(ImmutableBigInteger.fromNumber(1), gqGroup), GqElement.fromValue(ImmutableBigInteger.fromNumber(1), gqGroup));
			const firstVectorInverted = GroupVector.from<GqElement, GqGroup>(firstVector.elements.map(element => (element.invert())));

			const expected = GroupVector.of<GqElement, GqGroup>(GqElement.fromValue(ImmutableBigInteger.fromNumber(3), gqGroup), GqElement.fromValue(ImmutableBigInteger.fromNumber(5), gqGroup));

			return [
				{first: firstVector, second: secondVector, expected: expected},
				{first: firstVector, second: oneVector, expected: firstVector},
				{first: firstVector, second: firstVectorInverted, expected: oneVector},
				{first: oneVector, second: oneVector, expected: oneVector}
			];
		}

		test.each(multiplicationArgumentsProvider())("multiplication", ({first, second, expected}) => {
			const multiplication = vectorMultiplication(first, second);

			expect(multiplication.equals(expected)).toBe(true);
		});
	});

});

describe("vector exponentiate with", () => {

	let vectorSize: number;
	let vector: GroupVector<GqElement, GqGroup>;
	let exponent: ZqElement;

	beforeEach(() => {
		vectorSize = randomService.nextInt(MAX_LENGTH) + 1;
		vector = gqGroupGenerator.genRandomGqElementVector(vectorSize);
		exponent = zqGroupGenerator.genRandomZqElementMember();
	});

	test("valid parameters gives valid vector", () => {
		const product: GroupVector<GqElement, GqGroup> = vectorExponentiation(vector, exponent);

		expect(product.size).toEqual(vectorSize);
		expect(vector.group.equals(product.group)).toBe(true);
	});

	test("any null parameter throws NullPointerException", () => {
		expect(() => vectorExponentiation(null, exponent)).toThrow(NullPointerError);
		expect(() => vectorExponentiation(vector, null)).toThrow(NullPointerError);
	});

	describe("specific values gives expected vector exponentation", () => {
		interface Argument {
			vector: GroupVector<GqElement, GqGroup>;
			exponent: ZqElement;
			expected: GroupVector<GqElement, GqGroup>;
		}

		function exponentiateArgumentsProvider(): Argument[] {
			const p = ImmutableBigInteger.fromString("11");
			const q = ImmutableBigInteger.fromString("5");
			const g = ImmutableBigInteger.fromString("3");
			const gqGroup = new GqGroup(p, q, g);

			const oneExponent: ZqElement = ZqElement.create(1, ZqGroup.sameOrderAs(gqGroup));
			const zeroExponent: ZqElement = ZqElement.create(0, ZqGroup.sameOrderAs(gqGroup));

			const firstVector = GroupVector.of<GqElement, GqGroup>(GqElement.fromValue(ImmutableBigInteger.fromNumber(1), gqGroup), GqElement.fromValue(ImmutableBigInteger.fromNumber(4), gqGroup));
			const firstVectorNegated = GroupVector.of<GqElement, GqGroup>(GqElement.fromValue(ImmutableBigInteger.fromNumber(1), gqGroup).invert(), GqElement.fromValue(ImmutableBigInteger.fromNumber(4), gqGroup).invert());
			const fourExponent: ZqElement = ZqElement.create(4, ZqGroup.sameOrderAs(gqGroup));
			const threeExponent: ZqElement = ZqElement.create(4, ZqGroup.sameOrderAs(gqGroup));
			const firstVectorInverted = GroupVector.from<GqElement, GqGroup>(firstVector.elements
				.map(element => element.exponentiate(threeExponent))
				.map(exponentiatedElement => (exponentiatedElement.invert())));
			const oneVector = GroupVector.of<GqElement, GqGroup>(GqElement.fromValue(ImmutableBigInteger.fromNumber(1), gqGroup), GqElement.fromValue(ImmutableBigInteger.fromNumber(1), gqGroup));
			const expected = GroupVector.of<GqElement, GqGroup>(GqElement.fromValue(ImmutableBigInteger.fromNumber(1), gqGroup), GqElement.fromValue(ImmutableBigInteger.fromNumber(3), gqGroup));

			return [
				{vector: firstVector, exponent: fourExponent, expected: expected},
				{vector: firstVectorInverted, exponent: threeExponent, expected: firstVectorNegated},
				{vector: firstVector, exponent: oneExponent, expected: firstVector},
				{vector: firstVector, exponent: zeroExponent, expected: oneVector}
			];
		}

		test.each(exponentiateArgumentsProvider())("exponentiation", ({vector, exponent, expected}) => {
			const exponentiate = vectorExponentiation(vector, exponent);

			expect(exponentiate.equals(expected)).toBe(true);
		});
	});

});

describe("vector scalar multiplication with", () => {

	let vectorSize: number;
	let scalar: ZqElement;
	let vector: GroupVector<ZqElement, ZqGroup>;

	beforeEach(() => {
		vectorSize = randomService.nextInt(MAX_LENGTH) + 1;
		scalar = zqGroupGenerator.genRandomZqElementMember();
		vector = zqGroupGenerator.genRandomZqElementVector(vectorSize);
	});

	test("valid params gives valid multiplication", () => {
		const product: GroupVector<ZqElement, ZqGroup> = vectorScalarMultiplication(scalar, vector);

		expect(product.size).toBe(vectorSize);
		expect(vector.group.equals(product.group)).toBe(true);
	});

	test("any null parameter throws NullPointerException", () => {
		expect(() => vectorScalarMultiplication(null, vector)).toThrow(NullPointerError);
		expect(() => vectorScalarMultiplication(scalar, null)).toThrow(NullPointerError);
	});

	test("scalar of different group throws IllegalArgumentException", () => {
		const differentGroupScalar = otherZqGroupGenerator.genRandomZqElementMember();

		expect(() => vectorScalarMultiplication(differentGroupScalar, vector))
			.toThrow(new IllegalArgumentError("The scalar must be of the same group than the vector."));
	});

	describe("specific values gives expected vector scalar multiplication", () => {
		interface Argument {
			scalar: ZqElement;
			vector: GroupVector<ZqElement, ZqGroup>;
			expected: GroupVector<ZqElement, ZqGroup>;
		}

		function multiplicationArgumentsProvider(): Argument[] {
			const q = ImmutableBigInteger.fromString("5");
			const zqGroup = new ZqGroup(q);

			const scalar = ZqElement.create(3, zqGroup);
			const zeroScalar = ZqElement.create(0, zqGroup);
			const oneScalar = ZqElement.create(1, zqGroup);
			const vector = GroupVector.of<ZqElement, ZqGroup>(ZqElement.create(1, zqGroup), ZqElement.create(2, zqGroup));
			const zeroVector = GroupVector.of<ZqElement, ZqGroup>(ZqElement.create(0, zqGroup), ZqElement.create(0, zqGroup));

			const expected = GroupVector.of<ZqElement, ZqGroup>(ZqElement.create(3, zqGroup), ZqElement.create(1, zqGroup));

			return [
				{scalar: scalar, vector: vector, expected: expected},
				{scalar: zeroScalar, vector: vector, expected: zeroVector},
				{scalar: oneScalar, vector: vector, expected: vector},
			];
		}

		test.each(multiplicationArgumentsProvider())("vector scalar multiplication", ({scalar, vector, expected}) => {
			expect(expected.equals(vectorScalarMultiplication(scalar, vector))).toBeTruthy();
		});
	});

});
