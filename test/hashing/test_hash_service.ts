/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {HashService} from "../../src/hashing/hash_service";
import {byteArrayToInteger, integerToByteArray} from "../../src/conversions";
import {ImmutableUint8Array} from "../../src/immutable_uint8Array";
import {Hashable} from "../../src/hashing/hashable";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";

export class TestHashService extends HashService {

	private static DELEGATE_HASH_SERVICE = new HashService();

	private readonly lowerBoundInternal: ImmutableBigInteger;
	private readonly upperBoundInternal: ImmutableBigInteger;

	private constructor(lowerBound: ImmutableBigInteger, upperBound: ImmutableBigInteger) {
		super();
		this.lowerBoundInternal = lowerBound;
		this.upperBoundInternal = upperBound;
	}

	/**
	 * Creates a TestHashService whose output value is between {@code lowerBound} (incl.) and {@code upperBound} (excl.).
	 *
	 * @param upperBound the upper bound, exclusive.
	 * @param lowerBound the lower bound, inclusive. Defaults to 0.
	 * @return a TestHashService.
	 */
	public static create(upperBound: ImmutableBigInteger, lowerBound: ImmutableBigInteger = ImmutableBigInteger.ZERO): HashService {
		return new TestHashService(lowerBound, upperBound);
	}

	/**
	 * Perform the recursive hash, ensuring the output value is within the bounds.
	 *
	 * @param values the values to hash.
	 * @return the bounded hash of the {@code values}.
	 */
	public recursiveHash(...values: Hashable[]): ImmutableUint8Array {
		const hashValue = byteArrayToInteger(TestHashService.DELEGATE_HASH_SERVICE.recursiveHash(...values));
		const hashValueInBounds = hashValue.mod(this.upperBoundInternal.subtract(this.lowerBoundInternal)).add(this.lowerBoundInternal);

		return integerToByteArray(hashValueInBounds);
	}

	public getHashLength(): number {
		return Math.floor((this.upperBoundInternal.subtract(ImmutableBigInteger.ONE).bitLength()) / 8);
	}

}