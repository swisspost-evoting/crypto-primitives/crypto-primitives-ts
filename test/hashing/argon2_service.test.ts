/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {SecureRandomGenerator} from "../../src/generator/secure_random_generator";
import {Argon2Service} from "../../src/hashing/argon2_service";
import {Base64} from "js-base64";
import {ImmutableUint8Array} from "../../src/immutable_uint8Array";
import genArgon2idJson from './gen-argon2id.json';
import getArgon2idJson from './get-argon2id.json';
import {Argon2Profile} from "../../src/hashing/argon2_profile";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";

jest.mock("../../src/generator/secure_random_generator.ts")

let argon2Service: Argon2Service
const genRandomBytesMock = jest.fn();

beforeAll(() => {
	SecureRandomGenerator.genRandomBytes = genRandomBytesMock;
	argon2Service = new Argon2Service(Argon2Profile.TEST);
})


describe("genArgon2id with", () => {
	test("known values yield the expected result", async () => {
		const parameters = JSON.parse(JSON.stringify(genArgon2idJson));

		for (const x of parameters) {
			const context = x.context;
			argon2Service = new Argon2Service(getArgon2Profile(context.m, context.p, context.i));

			const salt = ImmutableUint8Array.from(Base64.toUint8Array(x.mocked.s));
			genRandomBytesMock.mockReturnValue(salt);

			const inputKeyingMaterial = ImmutableUint8Array.from(Base64.toUint8Array(x.input.k));
			const actualHash = await argon2Service.genArgon2id(inputKeyingMaterial);

			const expectedHash = Base64.toUint8Array(x.output.t);
			const expectedSalt = Base64.toUint8Array(x.output.s);

			expect(actualHash.tag.value()).toEqual(expectedHash);
			expect(actualHash.salt.value()).toEqual(expectedSalt);
		}
	})
})

describe("getArgon2id with", () => {
	test("known values yield the expected result", async () => {
		const parameters = JSON.parse(JSON.stringify(getArgon2idJson));

		for (const x of parameters) {
			const context = x.context;
			argon2Service = new Argon2Service(getArgon2Profile(context.m, context.p, context.i));

			const inputKeyingMaterial = ImmutableUint8Array.from(Base64.toUint8Array(x.input.k));
			const salt = ImmutableUint8Array.from(Base64.toUint8Array(x.input.s));
			const actualHash = await argon2Service.getArgon2id(inputKeyingMaterial, salt);

			const expectedHash = Base64.toUint8Array(x.output.t);

			expect(actualHash.value()).toEqual(expectedHash);
		}
	})
})

let getArgon2Profile = function (m: number, p: number, i: number): Argon2Profile {
	if (m == 21 && p == 4 && i == 1) {
		return Argon2Profile.STANDARD;
	} else if (m == 16 && p == 4 && i == 3) {
		return Argon2Profile.LESS_MEMORY;
	} else if (m == 14 && p == 4 && i == 1) {
		return Argon2Profile.TEST;
	} else {
		throw new IllegalArgumentError("Given arguments do not correspond to any known profile.");
	}
}
