/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {HashService} from "../../src/hashing/hash_service";
import {Base64} from 'js-base64';
import recursiveHashOfLengthJson from './recursive-hash-of-length.json';
import recursiveHashSHA256Json from './recursive-hash-sha3-256.json';
import recursiveHashZqJson from './recursive-hash-to-zq.json';
import {Hashable} from "../../src/hashing/hashable";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {SecureRandomGenerator} from "../../src/generator/secure_random_generator";
import {MessageDigest} from "../../src/hashing/message_digest";
import {ImmutableArray} from "../../src/immutable_array";
import {ImmutableUint8Array} from "../../src/immutable_uint8Array";
import {concat} from "../../src/arrays";
import {RandomService} from "../../src/math/random_service";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";
import {ZqElement} from "../../src/math/zq_element";
import {ZqGroup} from "../../src/math/zq_group";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {integerToByteArray, stringToByteArray} from "../../src/conversions";
import {GqElement} from "../../src/math/gq_element";
import {GqGroup} from "../../src/math/gq_group";

const TEST_INPUT_LENGTH = 5;

let secureRandom: SecureRandomGenerator;
let randomService: RandomService;
let hashService: HashService;
let messageDigest: MessageDigest;

beforeAll(() => {
	secureRandom = new SecureRandomGenerator();
	randomService = new RandomService();
	hashService = new HashService();
	messageDigest = new MessageDigest();
});


describe("recursiveHash", () => {
	describe("of specific input returns expected output", () => {
		const parameters = JSON.parse(JSON.stringify(recursiveHashSHA256Json));

		parameters.forEach((x: any) => {
			test(x.description, () => {
				const values: Hashable = readInput(x.input.values);

				const expectedHash: ImmutableUint8Array = ImmutableUint8Array.from(Base64.toUint8Array(x.output.hash));

				const actualHash: ImmutableUint8Array = hashService.recursiveHash(values);

				expect(expectedHash).toEqual<ImmutableUint8Array>(actualHash);
			})
		});
	});

	test("of byte array returns hash of byte array", () => {
		const bytes = SecureRandomGenerator.genRandomBytes(TEST_INPUT_LENGTH);

		const recursiveHash = hashService.recursiveHash(bytes);
		const regularHash = messageDigest.digest(concat(ImmutableUint8Array.from([0x00]), bytes));

		expect(regularHash).toEqual<ImmutableUint8Array>(recursiveHash);
	});

	test("of string returns hash of string", () => {
		const string = genRandomHashableString();
		const expected: ImmutableUint8Array = messageDigest.digest(concat(ImmutableUint8Array.from([0x02]), stringToByteArray(string)));
		const recursiveHash: ImmutableUint8Array = hashService.recursiveHash(string);

		expect(expected).toEqual<ImmutableUint8Array>(recursiveHash);
	});

	test("of a random BigInteger returns same hash of its integer value", () => {
		const bigInteger: ImmutableBigInteger = ImmutableBigInteger.random(3072, secureRandom);
		const recursiveHash: ImmutableUint8Array = hashService.recursiveHash(bigInteger);
		const regularHash: ImmutableUint8Array = messageDigest.digest(concat(ImmutableUint8Array.from([0x01]), integerToByteArray(bigInteger)));

		expect(regularHash).toEqual<ImmutableUint8Array>(recursiveHash);
	});

	test("of null throws", () => {
		expect(() => hashService.recursiveHash(null)).toThrow(new IllegalArgumentError("Values contain a null value which cannot be hashed."));
	});

	test("of array of one element does not equal hash of element", () => {
		const bytes: ImmutableUint8Array = SecureRandomGenerator.genRandomBytes(TEST_INPUT_LENGTH);
		const array: ImmutableArray<ImmutableUint8Array> = ImmutableArray.of(bytes);

		const expected: ImmutableUint8Array = hashService.recursiveHash(bytes);
		const hash: ImmutableUint8Array = hashService.recursiveHash(array);

		expect(expected).not.toEqual<ImmutableUint8Array>(hash);
	});

	test("of two byte arrays returns hash of concatenated individual hashes", () => {
		const bytes1: ImmutableUint8Array = SecureRandomGenerator.genRandomBytes(TEST_INPUT_LENGTH);
		const bytes2: ImmutableUint8Array = SecureRandomGenerator.genRandomBytes(TEST_INPUT_LENGTH);
		const array: ImmutableArray<ImmutableUint8Array> = ImmutableArray.of(bytes1, bytes2);

		const hash: ImmutableUint8Array = hashService.recursiveHash(array);

		const concatenation = concat(
			ImmutableUint8Array.from([0x03]), // Array prefix
			messageDigest.digest(concat(ImmutableUint8Array.from([0x00]), bytes1)),
			messageDigest.digest(concat(ImmutableUint8Array.from([0x00]), bytes2)));

		const expected: ImmutableUint8Array = messageDigest.digest(concatenation);

		expect(expected).toEqual<ImmutableUint8Array>(hash);
	});

	test("a byte array and an array of two byte arrays returns expected hash", () => {
		const bytes1: ImmutableUint8Array = SecureRandomGenerator.genRandomBytes(TEST_INPUT_LENGTH);
		const bytes2: ImmutableUint8Array = SecureRandomGenerator.genRandomBytes(TEST_INPUT_LENGTH);
		const bytes3: ImmutableUint8Array = SecureRandomGenerator.genRandomBytes(TEST_INPUT_LENGTH);
		const array: ImmutableArray<ImmutableUint8Array> = ImmutableArray.of(bytes2, bytes3);
		const input: ImmutableArray<Hashable> = ImmutableArray.of<Hashable>(bytes1, array);

		const hash: ImmutableUint8Array = hashService.recursiveHash(input);

		const subConcatenation: ImmutableUint8Array = concat(...array.map(a => messageDigest.digest(concat(ImmutableUint8Array.from([0x00]), a))).elements());
		const subHash: ImmutableUint8Array = messageDigest.digest(concat(ImmutableUint8Array.from([0x03]), subConcatenation));
		const concatenation: ImmutableUint8Array = concat(
			ImmutableUint8Array.from([0x03]), // Array prefix
			messageDigest.digest(concat(ImmutableUint8Array.from([0x00]), bytes1)), // Byte array hash
			subHash); // Hash of nested array
		const expected: ImmutableUint8Array = messageDigest.digest(concatenation);

		expect(expected).toEqual<ImmutableUint8Array>(hash);
	});

	test("of empty array throws", () => {
		const array: ImmutableArray<Hashable> = ImmutableArray.of();
		expect(() => hashService.recursiveHash(array)).toThrow(new IllegalArgumentError("Cannot hash an empty array."));
	});

	test("of nested empty array throws", () => {
		const emptyArray: ImmutableArray<Hashable> = ImmutableArray.of();
		const array: ImmutableArray<Hashable> = ImmutableArray.of<Hashable>(ImmutableBigInteger.ONE, emptyArray);

		expect(() => hashService.recursiveHash(array)).toThrow(new IllegalArgumentError("Cannot hash an empty array."))
	});

	test("of varargs gives same result as with array", () => {
		const first: ImmutableBigInteger = genRandomHashableBigInteger();
		const second: string = genRandomHashableString();
		const third = genRandomHashableByteArray();
		const array: ImmutableArray<ImmutableUint8Array> = ImmutableArray.of(third);
		const input: ImmutableArray<Hashable> = ImmutableArray.of<Hashable>(array, first, second);

		const varargsHash: ImmutableUint8Array = hashService.recursiveHash(array, first, second);
		const inputHash: ImmutableUint8Array = hashService.recursiveHash(input);

		expect(inputHash).toEqual<ImmutableUint8Array>(varargsHash);
	});

	test("of two inputs identical when concatenated but different when split do not collide", () => {
		for (let i = 0; i < 10; i++) {
			const size = randomService.nextInt(50) + 2;
			const concatenated: ImmutableUint8Array = SecureRandomGenerator.genRandomBytes(size);

			const first: Split = split(concatenated);
			let second: Split;
			do {
				second = split(concatenated);
			} while (arrayEquals(second.start, first.start));

			const firstHash: ImmutableUint8Array = hashService.recursiveHash(first.start, first.end);
			const secondHash: ImmutableUint8Array = hashService.recursiveHash(second.start, second.end);

			expect(firstHash).not.toEqual<ImmutableUint8Array>(secondHash);
		}
	});

	test("of similar characters hash to different values", () => {
		const first = "e";
		const firstHash: ImmutableUint8Array = hashService.recursiveHash(first);
		const second = "é";
		const secondHash: ImmutableUint8Array = hashService.recursiveHash(second);

		expect(firstHash).not.toEqual<ImmutableUint8Array>(secondHash);
	});

});


describe('recursiveHashOfLength', () => {
	test("of specific values returns expected result", () => {
		const parameters = JSON.parse(JSON.stringify(recursiveHashOfLengthJson));

		parameters.forEach((x: any) => {
			const requestedBitLength = x.input.requestedBitLength;
			const values: Hashable = readInput(x.input.values);

			const expectedHash: ImmutableUint8Array = integerToByteArray(ImmutableBigInteger.fromString(x.output.result.substring(2), 16));

			const actualHash: ImmutableUint8Array = hashService.recursiveHashOfLength(requestedBitLength, values);

			expect(expectedHash).toEqual(actualHash);
		});
	});

	test("of test string returns expected result", () => {
		const requestedBitLength = 2047;
		const values: string = "test string";

		const expectedHash: ImmutableUint8Array = integerToByteArray(ImmutableBigInteger.fromString("2506B7962D554D99D1BC3F2681224A99CAB3154E66FD91067D3843B03E5D2B945E0210D343363A9F646C89784DA2A81BB641804824D421080DFA5DD66784BE0C5106338FCBC3240CD096366106FD786B0A60C6F9471B41F2FC577576FE7C8D58C166F9B12CA90AF2812FF0FD838496D9768B31DFAC7D337FE498396D71DD8A005AC784C72E31DADB901B1CF39A3FDBEFFEE8673C29BD6EF628342C4BEC3E81DB8537A7170E160665D384E32D55C268747599400E1E187DA739E9513453D1B396A3D298626CD2AA2754052179436A225FD451CBF0C43A9A65FA6CAEFC934D90630034ECA40823F5FFE2B2458094E68D22D2C779CC7852F610B80F295215C40DBF", 16));

		const actualHash: ImmutableUint8Array = hashService.recursiveHashOfLength(requestedBitLength, values);

		expect(actualHash).toEqual(expectedHash);
	});

	test("of requestedLength null throws", () => {
		expect(() => hashService.recursiveHashOfLength(null, ImmutableBigInteger.ONE)).toThrow(NullPointerError);
	});

	test("of values null throws", () => {
		expect(() => hashService.recursiveHashOfLength(512, null)).toThrow(new IllegalArgumentError("Values contain a null value which cannot be hashed."));
	});

	test("of too small requestedLength throws", () => {
		expect(() => hashService.recursiveHashOfLength(511, ImmutableBigInteger.ONE)).toThrow(new IllegalArgumentError("The requested bit length must be at least 512."));
	});

	test("of empty array throws", () => {
		const array: ImmutableArray<Hashable> = ImmutableArray.of();
		expect(() => hashService.recursiveHashOfLength(512, array)).toThrow(new IllegalArgumentError("Cannot hash an empty array."));
	});

	test("of nested empty array throws", () => {
		const emptyArray: ImmutableArray<Hashable> = ImmutableArray.of();
		const array: ImmutableArray<Hashable> = ImmutableArray.of<Hashable>(ImmutableBigInteger.ONE, emptyArray);

		expect(() => hashService.recursiveHashOfLength(512, array)).toThrow(new IllegalArgumentError("Cannot hash an empty array."))
	});
});


describe("recursiveHashToZq", () => {
	describe("of specific input returns expected output", () => {
		const parameters = JSON.parse(JSON.stringify(recursiveHashZqJson));

		parameters.forEach((x: any) => {
			test(x.description, () => {
				const q: ImmutableBigInteger = ImmutableBigInteger.fromString(x.input.q.substring(2), 16);
				const values: Hashable = readInput(x.input.values);

				const expectedHash: ZqElement = ZqElement.create(ImmutableBigInteger.fromString(x.output.result.substring(2), 16), new ZqGroup(q));

				let actualHash: ZqElement;
				if (values instanceof ImmutableArray) {
					actualHash = hashService.recursiveHashToZq(q, ...values.elements());
				} else {
					actualHash = hashService.recursiveHashToZq(q, values);
				}

				expect(actualHash.equals(expectedHash)).toBeTruthy();
			})
		});
	});

	test("with null exclusiveUpperBound throws a NullPointerError", () => {
		expect(() => hashService.recursiveHashToZq(null, "")).toThrow(NullPointerError);
	});

	test("without hash values throws an IllegalArgumentError", () => {
		expect(() => hashService.recursiveHashToZq(ImmutableBigInteger.ONE)).toThrow(IllegalArgumentError);
	});

	test("with exclusiveUpperBound smaller than 1 throws an IllegalArgumentError", () => {
		expect(() => hashService.recursiveHashToZq(ImmutableBigInteger.ZERO, "")).toThrow(IllegalArgumentError);
	});

	test("with exclusiveUpperBound with bit size smaller than 512 throws an IllegalArgumentError", () => {
		let tooSmallExclusiveUpperBound: ImmutableBigInteger = ImmutableBigInteger.fromString("6703903964971298549787012499102923063739682910296196688861780721860882015036773488400937149083451713845015929093243025426876941405973284973216824503042047");
		expect(() => hashService.recursiveHashToZq(tooSmallExclusiveUpperBound, "")).toThrow(IllegalArgumentError);
	});
});


describe('hashAndSquare', () => {

	let group: GqGroup;
	let q: ImmutableBigInteger;

	beforeAll(() => {
		q = ImmutableBigInteger.fromNumber(11);
		const p: ImmutableBigInteger = ImmutableBigInteger.fromNumber(23);
		const g: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);

		group = new GqGroup(p, q, g);
	});

	test("of null throws NullPointerError", () => {
		expect(() => hashService.hashAndSquare(null, group)).toThrow(NullPointerError);
		expect(() => hashService.hashAndSquare(q, null)).toThrow(NullPointerError);
	});

	test("of too big hash length throws IllegalArgumentError", () => {
		expect(() => hashService.hashAndSquare(q, group)).toThrow(IllegalArgumentError);
	});

	test("of specific value gives expected result", () => {
		const x: ImmutableBigInteger = ImmutableBigInteger.fromString("4C80B8979704625337153559102FE4C8605CD7B897FC0B0F45B1B0951C4E2747753AD5B5B97E5EDBFE0EAED87CDA7996B5CE0463F22684971721AFE8B4582AA7A399992FE24B4EC85FBA1CBEF8FB73F2EC24F617D0D1408245D83583BDB68630134734B4A9567A3EC1C8DE499AFDC22F5C0F43448AFEDD8C58F8BBCE7D8D59B8AF92E223E97BB2A6E7C005B8AE1FAAC7CE06C61053A8A891F15888C0C514F2687A077F62E38985AB65976C7C13B8052E1A95C1F4506AA350FEFAAD3B7BF94162B87413CCF8B1FC48985D5B72291992D45392C63BAFB7A22E315EB47D6E5AD4964445D8DB9E9231BCDF6624320CF158E4CA077363B5AA95CE8178269487DADB7E", 16);

		const q: ImmutableBigInteger = ImmutableBigInteger.fromString("5FFB3E665707B0D9C5D3856B9B67D4751425AEB6575F97F697E446856FFCF159105FECE66D2CDE9DEA958966FE67A0D51ECDFC0FCAD3EACA293485FA2FBCC9DF3B055DE51F14B82EA39D3331C6E6B753C331E06DC8F1F0558EFF0D7F928C0EA6961DD02CFC898ECAE9BFA18919F5113B702964B06E58987CEFFEE05F4BBE4CA3F3D702F528B5540D92947F781B12D67E7A4AE1D5AEAF8BB703789C1574B52381908496060E0150CB55A6D1069B02DA73952E7E8B67C9C0E41A89F5E8C5452510DFCADC3276D26010A2C1F4CD18C07BD2B0F8CEA28DE21AA73D1426E3F5862D02EE2C42B636E4679D2BDA16C336C2FA29E8DEC663088BFDB035205785077BB6B01E3D183E05C42A1AAEAC1B3BA635D8911C704C033C15243DDCC44570EDAA6F651FF61BA698664D391698292C2834E9095B17EB3AC38819BE50BA08F417FBF3F3DBAA7A64F9D0E24D50AF0685074D82D17544010B68295BC07340B46519B184E9E0C01513C57E78E07C7D19C0E0A2ED0432449110DCB0766B6A30B2F02BDAAF75", 16);
		const p: ImmutableBigInteger = q.multiply(ImmutableBigInteger.fromNumber(2)).add(ImmutableBigInteger.ONE);
		const g: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const group: GqGroup = new GqGroup(p, q, g);

		const hashValue: ImmutableBigInteger = ImmutableBigInteger.fromString("A56EAFC2515E59E700F2D0A671B4ACAA22D1889FF0F0711F3F28B6FC7DB12A369DFA68543DADCB00AA3502EDD1B22E36CE8AC4B9594DD8058BD1D81203BAC467F7AF2E0A92B756AA9B847EB06385A31123D43B69CE3664D5EE30AF663993C79E6CA7B2450FFB7B553108C642EF9F4DD0118233E98B4E17F1E2712C4DB1D19209E9EEE05DCE7B4B4F8FCC1E540249969920897448B12578FD9311D115149077BB0B84F91CA66D9B19474F8F32D2F577B2E11634DEBDF39D02F05BCCA7E9011F7EF47638403ED4790CC1B24AEDF2405D9E41CC228BCCECCB5932A7655DF70118352674694A37C6367D637D3CA650823396CB966C4941051D72090017C04E42127FC1EFFDAB055988BC8D2D981C030B31C7652CEBAFF4EC3625F75B926DDEFB18B0AD010A45BAF5554677620106E283FCD088FAEBEE9A4C79E1998EAAA6D456149E05BBAF66C420CA98142C4A58CAFF3FDAEE8D1E5E96E3BE8B94D9ACF6345EEE5F2FF17A343DFBF112826FBA614000227E84F995E3F5117385EE483E64B795824B", 16);
		const expected: GqElement = GqElement.fromValue(hashValue, group);
		expect(hashService.hashAndSquare(x, group)).toEqual(expected);
	});
});


function readInput(input: any): Hashable {
	const values: Hashable[] = [];

	if (Array.isArray(input)) {
		input.forEach(value => {
			if (Array.isArray(value)) {
				values.push(readInput(value));
			} else {
				values.push(readValue(value))
			}
		});
	} else {
		values.push(readValue(input));
	}

	if (values.length == 1) { // Caution: RecursiveHash( (v) ) != RecursiveHash( v )
		return values[0];
	} else {
		return ImmutableArray.of(...values);
	}
}

function readValue(input: any): Hashable {
	switch (input.type) {
		case "bytes":
			return ImmutableUint8Array.from(Base64.toUint8Array(input.value));
		case "string":
			return input.value;
		case "integer":
			return ImmutableBigInteger.fromString(input.value.substring(2), 16);
		default:
			throw new IllegalArgumentError(`Unknown type: ${input.type}`);
	}
}

function genRandomHashableByteArray(): ImmutableUint8Array {
	const size = randomService.nextInt(500) + 1;
	return SecureRandomGenerator.genRandomBytes(size);
}

function genRandomHashableString(): string {
	return String.fromCharCode(...genRandomHashableByteArray().toArray());
}

function genRandomHashableBigInteger(): ImmutableBigInteger {
	return ImmutableBigInteger.random(50, secureRandom);
}

function arrayEquals(first: ImmutableUint8Array, second: ImmutableUint8Array): boolean {
	const expected = first.value();
	const actual = second.value();
	return expected.length === actual.length && expected.every((value, index) => value === actual[index]);
}

function split(input: ImmutableUint8Array): Split {
	const split = randomService.nextInt(input.length);
	const first: ImmutableUint8Array = ImmutableUint8Array.from(input.value().slice(0, split));
	const second: ImmutableUint8Array = ImmutableUint8Array.from(input.value().slice(split, input.length - split));

	return new Split(first, second);
}

class Split {
	readonly start;
	readonly end;

	constructor(start: ImmutableUint8Array, end: ImmutableUint8Array) {
		this.start = start;
		this.end = end;
	}
}

