/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ImmutableBigInteger} from "../src/immutable_big_integer";
import {VerificatumBigInteger as BigInteger} from "../src/verificatum_big_integer";
import {SecureRandomGenerator} from "../src/generator/secure_random_generator";

test("calling factories does not throw error", () => {
	const iString = ImmutableBigInteger.fromString("12", 10);
	const iNumber = ImmutableBigInteger.fromNumber(12);
	const iImmutableBigInteger = ImmutableBigInteger.copy(iNumber);

	expect(iString.equals(iNumber)).toBe(true);
	expect(iNumber.equals(iImmutableBigInteger)).toBe(true);

	const iRng = ImmutableBigInteger.random(iString.bitLength(), new SecureRandomGenerator());
	expect(iRng != null).toBe(true);
});

test("calling with default and specific radix", () => {
	const immutable = ImmutableBigInteger.fromNumber(16);
	const immutable2 = ImmutableBigInteger.fromString("10", 16);

	expect(immutable.equals(immutable2)).toBe(true);
});

test("to string(16) yields normalized hexadecimal representation", () => {
	const hexBigInt = ImmutableBigInteger.fromString("000D", 16);

	expect(hexBigInt.toString(16)).toEqual("0D")
});

test("delegate add method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);

	const expected: BigInteger = mutable.add(new BigInteger("2", 10));
	const actual :ImmutableBigInteger = immutable.add(ImmutableBigInteger.fromNumber(2));

	expect(expected.equals(actual.toBigInteger())).toBe(true);
});

test("delegate bitLength method return same result", () => {
	let mutable: BigInteger = new BigInteger("47", 10);
	const immutable = ImmutableBigInteger.fromNumber(47);

	expect(mutable.bitLength() === immutable.bitLength()).toBe(true);
});

test("delegate compareTo method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);

	const expected: number = mutable.compareTo(new BigInteger("2", 10));
	const actual :number = immutable.compareTo(ImmutableBigInteger.fromNumber(2));

	expect(expected === actual).toBe(true);
});

test("delegate gcd method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);

	const expected: BigInteger = mutable.gcd(new BigInteger("2", 10));
	const actual :ImmutableBigInteger = immutable.gcd(ImmutableBigInteger.fromNumber(2));

	expect(expected.equals(actual.toBigInteger())).toBe(true);
});

test("delegate intValue method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);

	const expected: number = mutable.intValue();
	const actual :number = immutable.intValue();

	expect(expected === actual).toBe(true);
});

test("delegate mod method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);

	const expected: BigInteger = mutable.mod(new BigInteger("5", 10));
	const actual :ImmutableBigInteger = immutable.mod(ImmutableBigInteger.fromNumber(5));

	expect(expected.equals(actual.toBigInteger())).toBe(true);
});

test("delegate modInverse method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);

	const expected: BigInteger = mutable.modInverse(new BigInteger("5"));
	const actual :ImmutableBigInteger = immutable.modInverse(ImmutableBigInteger.fromNumber(5));

	expect(expected.equals(actual.toBigInteger())).toBe(true);
});

test("delegate modPow method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);

	const expected: BigInteger = mutable.modPow(new BigInteger("5"), new BigInteger("7"));
	const actual :ImmutableBigInteger = immutable.modPow(ImmutableBigInteger.fromNumber(5), ImmutableBigInteger.fromNumber(7));

	expect(expected.equals(actual.toBigInteger())).toBe(true);
});

test("delegate multiply method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);

	const expected: BigInteger = mutable.multiply(new BigInteger("5"));
	const actual :ImmutableBigInteger = immutable.multiply(ImmutableBigInteger.fromNumber(5));

	expect(expected.equals(actual.toBigInteger())).toBe(true);
});

test("delegate negate method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);

	expect(mutable.negate().equals(immutable.negate().toBigInteger())).toBe(true);
});

test("delegate signum method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);

	expect(mutable.signum() === immutable.signum()).toBe(true);
});

test("delegate testBit method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	let mutable2: BigInteger = new BigInteger("7", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);
	const immutable2 = ImmutableBigInteger.fromNumber(7);

	expect(mutable.testBit(0)).toBe(false);
	expect(immutable.testBit(0)).toBe(false);
	expect(mutable2.testBit(0)).toBe(true);
	expect(immutable2.testBit(0)).toBe(true);
});

test("delegate toByteArray method return same result", () => {
	let mutable: BigInteger = new BigInteger("65658133689961323216879791121", 10);
	const immutable = ImmutableBigInteger.fromString("65658133689961323216879791121");

	const expected: number[] = mutable.toByteArray();
	const actual: number[] = immutable.toByteArray();

	expect(expected.every((e, i) => e === actual[i])).toBe(true);
});

test("delegate toString method return same result", () => {
	let mutable: BigInteger = new BigInteger("12", 10);
	const immutable = ImmutableBigInteger.fromNumber(12);

	expect(mutable.toString() === immutable.toString()).toBe(true);
});