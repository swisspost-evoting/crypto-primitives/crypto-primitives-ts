/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {SecureRandomGenerator} from "../src/generator/secure_random_generator";
import {ImmutableBigInteger} from "../src/immutable_big_integer";
import {integerToByteArray} from "../src/conversions";
import {ImmutableUint8Array} from "../src/immutable_uint8Array";

const secureRandomGenerator = new SecureRandomGenerator();

describe("Conversions implementations are equivalent with specification for", () => {
	test("integerToByteArray", () => {
		for (let i = 0; i < 1000; i++) {
			let bigInteger = ImmutableBigInteger.random(3072, secureRandomGenerator);

			expect(integerToByteArray(bigInteger)).toEqual(integerToByteArraySpec(bigInteger));
		}
	});

	function integerToByteArraySpec(integer: ImmutableBigInteger): ImmutableUint8Array {
		const TWOHUNDRED_FIFTY_SIX: ImmutableBigInteger = ImmutableBigInteger.fromNumber(256);
		let x: ImmutableBigInteger = integer;
		const n: number = byteLength(x);
		let B: Uint8Array = new Uint8Array(n);
		for (let i = 0; i < n; i++) {
			B.fill(x.mod(TWOHUNDRED_FIFTY_SIX).intValue(), n - i - 1, n - i);
			x = x.divide(TWOHUNDRED_FIFTY_SIX);
		}
		return ImmutableUint8Array.from(B);
	}

	function byteLength(x: ImmutableBigInteger): number {
		return Math.ceil(x.bitLength() / 8);
	}
});
