/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GqGroup} from "../../src/math/gq_group";
import {GqElement} from "../../src/math/gq_element";
import {ElGamalMultiRecipientCiphertext} from "../../src/elgamal/elgamal_multi_recipient_ciphertext";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";
import {ZqElement} from "../../src/math/zq_element";
import {ImmutableArray} from "../../src/immutable_array";
import {ZqGroup} from "../../src/math/zq_group";
import {ElGamalMultiRecipientMessage} from "../../src/elgamal/elgamal_multi_recipient_message";
import {ElGamalMultiRecipientPublicKey} from "../../src/elgamal/elgamal_multi_recipient_public_key";

import realValuesJson from './get-ciphertext.json';


describe("A ElGamalMultiRecipientCiphertext", () => {
	describe('with a valid other ciphertext gives expected result', () => {

		interface JsonFileArgument {
			messageList: GqElement[];
			exponent: ZqElement;
			publicKeyList: GqElement[];
			gammaRes: GqElement;
			phisRes: GqElement[];
			description: string;
		}

		function jsonFileArgumentProvider(): JsonFileArgument[] {
			const parametersList: any[] = JSON.parse(JSON.stringify(realValuesJson));


			const args: JsonFileArgument[] = [];
			parametersList.forEach(testParameters => {
				const p: ImmutableBigInteger = readValue(testParameters.context.p);
				const q: ImmutableBigInteger = readValue(testParameters.context.q);
				const g: ImmutableBigInteger = readValue(testParameters.context.g);

				const gqGroup: GqGroup = new GqGroup(p, q, g);
				const zqGroup: ZqGroup = new ZqGroup(q);

				const messageList: GqElement[] = readValues(testParameters.input.bold_m).map(m => GqElement.fromValue(m, gqGroup)).elements();

				const r: ImmutableBigInteger = readValue(testParameters.input.r);
				const exponent: ZqElement = ZqElement.create(r, zqGroup);

				const publicKey: GqElement[] = readValues(testParameters.input.bold_pk).map(pk => GqElement.fromValue(pk, gqGroup)).elements();

				const gammaOutput: ImmutableBigInteger = readValue(testParameters.output.gamma);
				const gamma: GqElement = GqElement.fromValue(gammaOutput, gqGroup)

				const phis: GqElement[] = readValues(testParameters.output.phis).map(phi => GqElement.fromValue(phi, gqGroup)).elements();

				args.push({
					messageList: messageList,
					exponent: exponent,
					publicKeyList: publicKey,
					gammaRes: gamma,
					phisRes: phis,
					description: testParameters.description
				});
			});

			return args;
		}

		// @ts-ignore description is used by test definition
		test.each(jsonFileArgumentProvider())("$description", ({messageList, exponent, publicKeyList, gammaRes, phisRes, description}) => {

			// Create first ciphertext.
			const message: ElGamalMultiRecipientMessage = new ElGamalMultiRecipientMessage(messageList);

			// Create second ciphertext.
			const publicKey: ElGamalMultiRecipientPublicKey = new ElGamalMultiRecipientPublicKey(publicKeyList);

			// Expected multiplication result.
			const ciphertextRes: ElGamalMultiRecipientCiphertext = ElGamalMultiRecipientCiphertext.create(gammaRes, phisRes);

			expect(ciphertextRes.equals(ElGamalMultiRecipientCiphertext.getCiphertext(message, exponent, publicKey)));
		});
	});
});

function readValues(input: string[]): ImmutableArray<ImmutableBigInteger> {
	const values: ImmutableBigInteger[] = [];
	input.forEach(value => values.push(readValue(value)));
	return ImmutableArray.from(values);
}

function readValue(value: string): ImmutableBigInteger {
	return ImmutableBigInteger.fromString(value.substring(2), 16);
}
