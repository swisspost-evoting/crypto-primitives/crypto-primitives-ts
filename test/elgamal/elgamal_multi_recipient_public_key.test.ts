/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GqGroup} from "../../src/math/gq_group";
import {GqElement} from "../../src/math/gq_element";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {GqGroupGenerator} from "../tools/gq_group_generator";
import {ElGamalMultiRecipientPublicKey} from "../../src/elgamal/elgamal_multi_recipient_public_key";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";

let generator: GqGroupGenerator;

function initializeGenerator() {
	const p: ImmutableBigInteger = ImmutableBigInteger.fromNumber(23);
	const q: ImmutableBigInteger = ImmutableBigInteger.fromNumber(11);
	const g: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);

	const gqGroup = new GqGroup(p, q, g);
	generator = new GqGroupGenerator(gqGroup);
}

beforeAll(() => {
	initializeGenerator();
});

describe("A multi-recipient public key", () => {
	test("contains the correct public key", () => {
		const m1: GqElement = generator.genMember();
		const m2: GqElement = generator.genMember();

		const validKeyElements: GqElement[] = [];
		validKeyElements.push(m1);
		validKeyElements.push(m2);
		const publicKey: ElGamalMultiRecipientPublicKey = new ElGamalMultiRecipientPublicKey(validKeyElements);

		expect(validKeyElements.every((keyElement, index) => keyElement.equals(publicKey.get(index)))).toBe(true);
	});

	describe('created with invalid parameters', () => {
		interface InvalidArgument {
			keyElements: GqElement[];
			exception: Error;
		}

		// Provides invalid parameters.
		function createInvalidArgumentsProvider(): InvalidArgument[] {

			initializeGenerator();

			const keyElementsFirstNull: GqElement[] = [];
			keyElementsFirstNull.push(null);
			keyElementsFirstNull.push(generator.genMember());

			const keyElementsSecondNull: GqElement[] = [];
			keyElementsSecondNull.push(generator.genMember());
			keyElementsSecondNull.push(null);

			const otherP: ImmutableBigInteger = ImmutableBigInteger.fromNumber(47);
			const otherQ: ImmutableBigInteger = ImmutableBigInteger.fromNumber(23);
			const otherG: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
			const otherGroup: GqGroup = new GqGroup(otherP, otherQ, otherG);

			const keyElementsDifferentGroups: GqElement[] = [];
			keyElementsDifferentGroups.push(generator.genMember());
			const otherGenerator: GqGroupGenerator = new GqGroupGenerator(otherGroup);
			keyElementsDifferentGroups.push(otherGenerator.genMember());

			return [
				{keyElements: null, exception: new NullPointerError()},
				{keyElements: [], exception: new IllegalArgumentError("An ElGamal public key must not be empty.")},
				{keyElements: keyElementsFirstNull, exception: new IllegalArgumentError("Elements must not contain nulls")},
				{keyElements: keyElementsSecondNull, exception: new IllegalArgumentError("Elements must not contain nulls")},
				{keyElements: keyElementsDifferentGroups, exception: new IllegalArgumentError("All elements must belong to the same group.")}
			]
		}

		test.each(createInvalidArgumentsProvider())("throws $exception", ({keyElements, exception}) => {
			expect(() => new ElGamalMultiRecipientPublicKey(keyElements)).toThrow(exception);
		});
	});
});