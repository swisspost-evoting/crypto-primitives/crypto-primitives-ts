/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GqGroup} from "../../src/math/gq_group";
import {GqElement} from "../../src/math/gq_element";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {GqGroupGenerator} from "../tools/gq_group_generator";
import {ElGamalMultiRecipientCiphertext} from "../../src/elgamal/elgamal_multi_recipient_ciphertext";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";

let gqGroup: GqGroup;
let generator: GqGroupGenerator;
let validGamma: GqElement;
let validPhis: GqElement[];

function initializeTest() {
	const p: ImmutableBigInteger = ImmutableBigInteger.fromNumber(23);
	const q: ImmutableBigInteger = ImmutableBigInteger.fromNumber(11);
	const g: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);

	gqGroup = new GqGroup(p, q, g);
	generator = new GqGroupGenerator(gqGroup);

	// Generate valid phis.
	const m1: GqElement = generator.genMember();
	const m2: GqElement = generator.genMember();

	validPhis = [];
	validPhis.push(m1);
	validPhis.push(m2);

	// Generate a valid gamma.
	do {
		validGamma = generator.genNonIdentityMember();
	} while (validGamma.equals(gqGroup.generator));
}

beforeEach(() => {
	initializeTest();
});

describe("A ElGamalMultiRecipientCiphertext", () => {
	test("contains the correct gamma and phis", () => {
		const ciphertext: ElGamalMultiRecipientCiphertext = ElGamalMultiRecipientCiphertext.create(validGamma, validPhis);
		expect(validGamma.equals(ciphertext.gamma)).toBe(true);
		expect(gqGroup.equals(ciphertext.group)).toBe(true);
		expect(validPhis.length === ciphertext.size).toBe(true);
		expect(validPhis.every((phisElement, index) => phisElement.equals(ciphertext.get(index)))).toBe(true);
	});

	describe('created with invalid parameters', () => {

		interface InvalidArgument {
			gamma: GqElement;
			phis: GqElement[];
			exception: Error;
		}

		function createInvalidArgumentsProvider(): InvalidArgument[] {

			initializeTest();

			const invalidPhis: GqElement[] = [GqElement.fromValue(ImmutableBigInteger.ONE, gqGroup), null];

			const otherP: ImmutableBigInteger = ImmutableBigInteger.fromNumber(47);
			const otherQ: ImmutableBigInteger = ImmutableBigInteger.fromNumber(23);
			const otherG: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
			const otherGroup: GqGroup = new GqGroup(otherP, otherQ, otherG);
			const otherGenerator: GqGroupGenerator = new GqGroupGenerator(otherGroup);
			const differentGroupPhis: GqElement[] = [generator.genMember(), otherGenerator.genMember()];

			const otherGroupGamma: GqElement = genOtherGroupGamma(otherGroup);

			return [
				{gamma: null, phis: validPhis, exception: new NullPointerError()},
				{gamma: validGamma, phis: null, exception: new NullPointerError()},
				{gamma: validGamma, phis: [], exception: new IllegalArgumentError("An ElGamalMultiRecipientCiphertext phis must be non empty.")},
				{gamma: validGamma, phis: invalidPhis, exception: new IllegalArgumentError("Elements must not contain nulls")},
				{gamma: validGamma, phis: differentGroupPhis, exception: new IllegalArgumentError("All elements must belong to the same group.")},
				{gamma: otherGroupGamma, phis: validPhis, exception: new IllegalArgumentError("Gamma and phis must belong to the same GqGroup.")}
			]
		}

		test.each(createInvalidArgumentsProvider())("throws $exception", ({gamma, phis,  exception}) => {
			expect(() => ElGamalMultiRecipientCiphertext.create(gamma, phis)).toThrow(exception);
		});
	});
});


function genOtherGroupGamma(otherGroup: GqGroup): GqElement {
	const otherGroupGenerator: GqGroupGenerator = new GqGroupGenerator(otherGroup);

	let otherGroupGamma: GqElement;
	do {
		otherGroupGamma = otherGroupGenerator.genNonIdentityMember();
	} while (otherGroupGamma.equals(otherGroup.generator));
	return otherGroupGamma;
}

