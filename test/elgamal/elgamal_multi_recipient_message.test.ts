/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GqGroup} from "../../src/math/gq_group";
import {GqElement} from "../../src/math/gq_element";
import {ElGamalMultiRecipientMessage} from "../../src/elgamal/elgamal_multi_recipient_message";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {GqGroupGenerator} from "../tools/gq_group_generator";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";

let generator: GqGroupGenerator;

function initializeTest() {
	const p: ImmutableBigInteger = ImmutableBigInteger.fromNumber(23);
	const q: ImmutableBigInteger = ImmutableBigInteger.fromNumber(11);
	const g: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);

	const gqGroup = new GqGroup(p, q, g);
	generator = new GqGroupGenerator(gqGroup);
}

beforeAll(() => {
	initializeTest();
});

interface InvalidArgument {
	messageElements: GqElement[];
	exception: Error;
}

describe("A ElGamalMultiRecipientMessage", () => {
	test("contains the correct message", () => {
		const m1: GqElement = generator.genMember();
		const m2: GqElement = generator.genMember();

		const validMessageElements: GqElement[] = [];
		validMessageElements.push(m1);
		validMessageElements.push(m2);
		const message: ElGamalMultiRecipientMessage = new ElGamalMultiRecipientMessage(validMessageElements);

		expect(validMessageElements.every((messageElement, index) => messageElement.equals(message.get(index)))).toBe(true);
	});


	function createInvalidArgumentsProvider(): InvalidArgument[] {

		initializeTest();

		const messageElementsFirstNull: GqElement[] = [];
		messageElementsFirstNull.push(null);
		messageElementsFirstNull.push(generator.genMember());

		const messageElementsSecondNull: GqElement[] = [];
		messageElementsSecondNull.push(generator.genMember());
		messageElementsSecondNull.push(null);

		const otherP: ImmutableBigInteger = ImmutableBigInteger.fromNumber(47);
		const otherQ: ImmutableBigInteger = ImmutableBigInteger.fromNumber(23);
		const otherG: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const otherGroup: GqGroup = new GqGroup(otherP, otherQ, otherG);

		const messageElementsDifferentGroups: GqElement[] = [];
		messageElementsDifferentGroups.push(generator.genMember());
		const otherGenerator: GqGroupGenerator = new GqGroupGenerator(otherGroup);
		messageElementsDifferentGroups.push(otherGenerator.genMember());

		return [
			{messageElements: null, exception: new NullPointerError()},
			{messageElements: [], exception: new IllegalArgumentError("An ElGamal message must not be empty.")},
			{messageElements: messageElementsFirstNull, exception: new IllegalArgumentError("Elements must not contain nulls")},
			{messageElements: messageElementsSecondNull, exception: new IllegalArgumentError("Elements must not contain nulls")},
			{messageElements: messageElementsDifferentGroups, exception: new IllegalArgumentError("All elements must belong to the same group.")}
		]
	}

	describe('created with invalid parameters', () => {
		test.each(createInvalidArgumentsProvider())("throws $exception", ({messageElements, exception}) => {
			expect(() => new ElGamalMultiRecipientMessage(messageElements)).toThrow(exception);
		});
	});
});

