/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {TestGroup} from "./test_group";
import {GroupElement} from "../../../src/math/group_element";
import {ImmutableBigInteger} from "../../../src/immutable_big_integer";

export class TestValuedElement extends GroupElement<TestGroup>{
	constructor(value: ImmutableBigInteger, group: TestGroup) {
		super(value, group);
	}
}