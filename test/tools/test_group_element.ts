/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GroupVectorElement} from "../../src/group_vector_element";
import {TestGroup} from "./math/test_group";
import {Hashable} from "../../src/hashing/hashable";

export class TestGroupElement implements GroupVectorElement<TestGroup> {
	private readonly groupInternal: TestGroup;

	constructor(group: TestGroup) {
		this.groupInternal = group;
	}

	get group(): TestGroup {
		return this.groupInternal;
	}

	get size(): number {
		return 1;
	}

	equals(o: GroupVectorElement<TestGroup>): boolean {
		if (this === o) {
			return true;
		}
		if (o == null || typeof this !== typeof o) {
			return false;
		}

		return this.groupInternal.equals(o.group);
	}

	toHashableForm(): Hashable {
		return "hashableForm";
	}
}
