/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GqGroup} from "../../src/math/gq_group";
import {SecureRandomGenerator} from "../../src/generator/secure_random_generator";
import {GqElement} from "../../src/math/gq_element";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";
import {GroupVector} from "../../src/group_vector";
import {verificatum} from "vts";
import RandomSource = verificatum.base.RandomSource;

export class GqGroupGenerator {
	private readonly groupInternal: GqGroup;
	private readonly randomInternal: RandomSource;

	constructor(group: GqGroup) {
		this.groupInternal = group;
		this.randomInternal = new SecureRandomGenerator()
	}

	/**
	 * Generate a BigInteger value that does not belong to the group.
	 */
	public genNonMemberValue(): ImmutableBigInteger {
		let nonMember: ImmutableBigInteger;
		do {
			nonMember = this.randomBigInteger(this.groupInternal.p.bitLength());
		} while (nonMember.compareTo(ImmutableBigInteger.ZERO) <= 0 || nonMember.compareTo(
			this.groupInternal.p) >= 0 || this.groupInternal.isGroupMember(nonMember));
		return nonMember;
	}

	/**
	 * Generate a BigInteger value that belongs to the group.
	 */
	public genMemberValue(): ImmutableBigInteger {
		let member: ImmutableBigInteger;
		do {
			const randomInteger: ImmutableBigInteger = this.randomBigInteger(this.groupInternal.p.bitLength());
			member = randomInteger.modPow(ImmutableBigInteger.fromNumber(2), this.groupInternal.p);
		} while (member.compareTo(ImmutableBigInteger.ZERO) <= 0 || member.compareTo(this.groupInternal.p) >= 0);
		return member;
	}

	/**
	 * Generate a GqElement belonging to the group.
	 */
	public genMember(): GqElement {
		return GqElement.fromValue(this.genMemberValue(), this.groupInternal);
	}

	/**
	 * Generate a non identity member of the group.
	 */
	public genNonIdentityMember(): GqElement {
		let nonIdentityMember: GqElement;
		do {
			nonIdentityMember = this.genMember();
		} while (nonIdentityMember.equals(this.groupInternal.identity));
		return nonIdentityMember;
	}

	/**
	 * Generate a random {@link GroupVector} of {@link GqElement} in this {@code group}.
	 *
	 * @param numElements the number of elements to generate.
	 * @return a vector of {@code numElements} random {@link GqElement}.
	 */
	public genRandomGqElementVector(numElements: number): GroupVector<GqElement, GqGroup> {
		const vector: GqElement[] = [];
		vector.length = numElements;

		for (let i = 0; i < numElements; i++) {
			vector[i] = this.genMember();
		}

		return GroupVector.from(vector);
	}

	private randomBigInteger(bitLength: number): ImmutableBigInteger {
		return ImmutableBigInteger.random(bitLength, this.randomInternal);
	}
}
