/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ZqGroup} from "../../../src/math/zq_group";
import {ZqElement} from "../../../src/math/zq_element";
import {RandomService} from "../../../src/math/random_service";
import {GroupVector} from "../../../src/group_vector";
import {ImmutableBigInteger} from "../../../src/immutable_big_integer";

export class ZqGroupGenerator {

	private readonly groupInternal: ZqGroup;
	private readonly randomServiceInternal: RandomService;

	constructor(group: ZqGroup) {
		this.groupInternal = group;
		this.randomServiceInternal = new RandomService();
	}

	public genRandomZqElementMember(): ZqElement {
		let value: ImmutableBigInteger = this.randomServiceInternal.genRandomInteger(this.groupInternal.q);
		return ZqElement.create(value, this.groupInternal);
	}

	/**
	 * Generate a random {@link GroupVector} of {@link ZqElement} in this {@code group}.
	 *
	 * @param numElements the number of elements to generate.
	 * @return a vector of {@code numElements} random {@link ZqElement}.
	 */
	public genRandomZqElementVector(numElements: number): GroupVector<ZqElement, ZqGroup> {
		const vector: ZqElement[] = [];
		vector.length = numElements;

		for (let i = 0; i < numElements; i++) {
			vector[i] = this.genRandomZqElementMember();
		}

		return GroupVector.from(vector);
	}

	public otherElement(element: ZqElement): ZqElement {
		let member: ZqElement;
		do {
			member = this.genRandomZqElementMember();
		} while (member.equals(element));

		return member;
	}

	public otherVector(vector: GroupVector<ZqElement, ZqGroup>): GroupVector<ZqElement, ZqGroup> {
		let otherVector: GroupVector<ZqElement, ZqGroup>;
		do {
			otherVector = this.genRandomZqElementVector(vector.size);
		} while (otherVector.equals(vector));

		return otherVector;
	}

}
