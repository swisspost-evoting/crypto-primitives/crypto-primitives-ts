/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GqGroup} from "../../../src/math/gq_group";
import {ImmutableArray} from "../../../src/immutable_array";
import {RandomService} from "../../../src/math/random_service";
import {ImmutableBigInteger} from "../../../src/immutable_big_integer";

const random: RandomService = new RandomService();

const group1: GqGroup = new GqGroup(ImmutableBigInteger.fromNumber(11), ImmutableBigInteger.fromNumber(5), ImmutableBigInteger.fromNumber(3));
const group2: GqGroup = new GqGroup(ImmutableBigInteger.fromNumber(23), ImmutableBigInteger.fromNumber(11), ImmutableBigInteger.fromNumber(2));
const group3: GqGroup = new GqGroup(ImmutableBigInteger.fromNumber(47), ImmutableBigInteger.fromNumber(23), ImmutableBigInteger.fromNumber(2));
const group4: GqGroup = new GqGroup(ImmutableBigInteger.fromNumber(59), ImmutableBigInteger.fromNumber(29), ImmutableBigInteger.fromNumber(3));

const smallTestGroups: ImmutableArray<GqGroup> = ImmutableArray.of(group1, group2, group3, group4);

export const GroupTestData = {

	getGroup(): GqGroup {
		return getRandomGqGroupFrom(smallTestGroups);
	},

	/**
	 * Get a different group from the provided {@code gqGroup}.
	 *
	 * @param gqGroup the group for which to get a different one.
	 * @return a different {@link GqGroup}.
	 */
	getDifferentGqGroup(gqGroup: GqGroup): GqGroup {
		const otherGroups = smallTestGroups.filter(_ => !_.equals(gqGroup));
		return getRandomGqGroupFrom(otherGroups);
	}

}

function getRandomGqGroupFrom(groups: ImmutableArray<GqGroup>): GqGroup {
	return groups.get(random.nextInt(groups.length));
}