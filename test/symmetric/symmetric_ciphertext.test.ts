/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {SymmetricCiphertext} from "../../src/symmetric/symmetric_ciphertext";
import {ImmutableUint8Array} from "../../src/immutable_uint8Array";
import {NullPointerError} from "../../src/error/null_pointer_error";

describe('Building a SymmetricCiphertext', () => {
	test("with null parameters throws a NullPointerError", () => {
		const ciphertext: ImmutableUint8Array = ImmutableUint8Array.from([1]);
		const nonce: ImmutableUint8Array = ImmutableUint8Array.from([2]);
		expect(() => new SymmetricCiphertext(null, nonce)).toThrow(NullPointerError);
		expect(() => new SymmetricCiphertext(ciphertext, null)).toThrow(NullPointerError);
	});
});