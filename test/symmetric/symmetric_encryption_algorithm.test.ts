/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {SymmetricEncryptionAlgorithm} from "../../src/symmetric/symmetric_encryption_algorithm";

describe("Getting a SymmetricEncryptionAlgorithm instance with", () => {
	test("AES256_GCM_NOPADDING contains the correct values", () => {
		const aes: SymmetricEncryptionAlgorithm = SymmetricEncryptionAlgorithm.AES256_GCM_NOPADDING();
		expect(aes.algorithmName === "aes-256-gcm").toBe(true);
		expect(aes.nonceLength === 12).toBe(true);
	});
});