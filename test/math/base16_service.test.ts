/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Base16Service} from "../../src/math/base16_service";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {SecureRandomGenerator} from "../../src/generator/secure_random_generator";

const base16Service = new Base16Service();

interface Argument {
  input: Uint8Array,
  expectedOutput: string
}

function getInputsAndOutputs(): Argument[] {
  return [
    {input: Uint8Array.of(), expectedOutput: ""},
    {input: Uint8Array.of(65), expectedOutput: "41"},
    {input: Uint8Array.of(96), expectedOutput: "60"},
    {input: Uint8Array.of(0), expectedOutput: "00"},
    {input: Uint8Array.of(127), expectedOutput: "7F"},
    {input: Uint8Array.of(-128), expectedOutput: "80"},
    {input: Uint8Array.of(-1), expectedOutput: "FF"},
    {input: Uint8Array.of(65, 0), expectedOutput: "4100"},
    {input: Uint8Array.of(1, 1, 1), expectedOutput: "010101"},
    {input: Uint8Array.of(127, 0, -2, 3), expectedOutput: "7F00FE03"},
  ];
}

describe('Base16Encode with', () => {

  test.each(getInputsAndOutputs())("valid input $input gives output $expectedOutput", ({input, expectedOutput}) => {
    const result: string = base16Service.base16Encode(input);

    expect(result).toEqual(expectedOutput);
  });

});

describe('Base16Decode with', () => {

  test.each(getInputsAndOutputs())("valid input $expectedOutput gives output $input", ({input, expectedOutput}) => {
    const result: Uint8Array = base16Service.base16Decode(expectedOutput);

    expect(result).toEqual(input);
  });

  function getInvalidStrings(): string[] {
    return ["ABC!", "?sss", "FO OO", "A", "aa", "AAA", "0G"];
  }

  test.each(getInvalidStrings())("invalid strings %s throws an IllegalArgumentError", invalidString => {
    expect(() => base16Service.base16Decode(invalidString))
      .toThrow(new IllegalArgumentError("The given string is not a valid Base16 string."));
  });

});

test("base16Encode then base16Decode returns initial value", () => {
  for (let i = 0; i < 10; i++) {
    const randomBytes: Uint8Array = SecureRandomGenerator.genRandomBytes(16).value();

    const string: string = base16Service.base16Encode(randomBytes);
    const result: Uint8Array = base16Service.base16Decode(string);
    expect(result).toEqual(randomBytes);
  }
});

test("Base16Decode with maxLength string", () => {
  const randomBytes: Uint8Array = SecureRandomGenerator.genRandomBytes(300).value();
  const string: string = base16Service.base16Encode(randomBytes);
  const result: Uint8Array = base16Service.base16Decode(string);
  expect(result).toEqual(randomBytes);
});
