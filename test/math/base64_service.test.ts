/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Base64Service} from "../../src/math/base64_service";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {SecureRandomGenerator} from "../../src/generator/secure_random_generator";

const base64Service = new Base64Service();

interface Argument {
	input: Uint8Array,
	expectedOutput: string
}

function getInputsAndOutputs(): Argument[] {
	return [
		{input: Uint8Array.of(), expectedOutput: ""},
		{input: Uint8Array.of(65), expectedOutput: "QQ=="},
		{input: Uint8Array.of(96), expectedOutput: "YA=="},
		{input: Uint8Array.of(0), expectedOutput: "AA=="},
		{input: Uint8Array.of(127), expectedOutput: "fw=="},
		{input: Uint8Array.of(-128), expectedOutput: "gA=="},
		{input: Uint8Array.of(-1), expectedOutput: "/w=="},
		{input: Uint8Array.of(65, 0), expectedOutput: "QQA="},
		{input: Uint8Array.of(1, 1, 1), expectedOutput: "AQEB"},
		{input: Uint8Array.of(127, 0, -2, 3), expectedOutput: "fwD+Aw=="},
	];
}

describe('Base64Encode with', () => {

	test.each(getInputsAndOutputs())("valid input $input gives output $expectedOutput", ({input, expectedOutput}) => {
		const result: string = base64Service.base64Encode(input);

		expect(result).toEqual(expectedOutput);
	});

});

describe('Base64Decode with', () => {

	test.each(getInputsAndOutputs())("valid input $expectedOutput gives output $input", ({input, expectedOutput}) => {
		const result: Uint8Array = base64Service.base64Decode(expectedOutput);

		expect(result).toEqual(input);
	});

	function getInvalidStrings(): string[] {
		return ["A=", "?sss", "abcdefg", "Inv=====", "x-y."];
	}

	test.each(getInvalidStrings())("invalid strings %s throws an IllegalArgumentError", invalidString => {
		expect(() => base64Service.base64Decode(invalidString))
			.toThrow(new IllegalArgumentError("The given string is not a valid Base64 string."));
	});

});

test("base64Encode then base64Decode returns initial value", () => {
	for (let i = 0; i < 10; i++) {
		const randomBytes: Uint8Array = SecureRandomGenerator.genRandomBytes(16).value();

		const string: string = base64Service.base64Encode(randomBytes);
		const result: Uint8Array = base64Service.base64Decode(string);
		expect(result).toEqual(randomBytes);
	}
});
