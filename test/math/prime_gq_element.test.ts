/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ImmutableBigInteger} from "../../src/immutable_big_integer";
import {GqGroup} from "../../src/math/gq_group";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {PrimeGqElement} from "../../src/math/prime_gq_element";
import {GqElement} from "../../src/math/gq_element";
import {ZqGroup} from "../../src/math/zq_group";
import {ZqElement} from "../../src/math/zq_element";
import {UnsupportedOperationError} from "../../src/error/unsupported_operation_error";

let g: ImmutableBigInteger;
let group: GqGroup;

beforeAll(() => {
	const p: ImmutableBigInteger = ImmutableBigInteger.fromNumber(59);
	const q: ImmutableBigInteger = ImmutableBigInteger.fromNumber(29);
	g = ImmutableBigInteger.fromNumber(3);

	group = new GqGroup(p, q, g);
});

describe('PrimeGqElement fromValue validation', () => {
	test("Given a value when a group element is created with that value then has that value", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(5);
		const element: PrimeGqElement = PrimeGqElement.fromValue(value.intValue(), group);
		expect(value.equals(element.value)).toBe(true);
	});

	test("When create an element with value zero then error", () => {
		expect(() => PrimeGqElement.fromValue(0, group)).toThrow(IllegalArgumentError);
	});

	test("When create an element with negative value then error", () => {
		expect(() => PrimeGqElement.fromValue(-1, group)).toThrow(IllegalArgumentError);
	});

	test("When create an element with null value then error", () => {
		expect(() => PrimeGqElement.fromValue(null, group)).toThrow(NullPointerError);
	});

	test("When create an element with null group then error", () => {
		expect(() => PrimeGqElement.fromValue(5, null)).toThrow(NullPointerError);
	});

	test("When create an element not member of the group then error", () => {
		expect(() => PrimeGqElement.fromValue(13, group)).toThrow(IllegalArgumentError);
	});

	test("When create an element that is not prime then error", () => {
		expect(() => PrimeGqElement.fromValue(4, group)).toThrow(IllegalArgumentError);
	});
})

describe('Multiplication method validations', () => {
	test("Given null element when multiply then exception", () => {
		const element1: PrimeGqElement = PrimeGqElement.fromValue(5, group);
		expect(() => element1.multiply(null)).toThrow(NullPointerError);
	});

	test("Given two elements from different groups when multiply then exception", () => {
		const element1: PrimeGqElement = PrimeGqElement.fromValue(5, group);
		const element2: PrimeGqElement = PrimeGqElement.fromValue(5, new GqGroup(ImmutableBigInteger.fromNumber(11), ImmutableBigInteger.fromNumber(5), g));

		expect(() => element1.multiply(element2)).toThrow(IllegalArgumentError);
	});

	test("Given two elements when multiplied then succeeds", () => {
		multiplyAndAssert(5, 7, 35);
	});

	test("Given two elements when multiplied then the result is smaller than p", () => {
		multiplyAndAssert(7, 29, 26);
	});

});


describe('Exponentiation method validations', () => {
	test("Given element and null exponent when exponentiate then exception", () => {
		const element: PrimeGqElement = PrimeGqElement.fromValue(5, group);

		expect(() => element.exponentiate(null)).toThrow(NullPointerError);
	});

	test("Given element and exponent from different groups when exponentiate then exception", () => {
		const element: PrimeGqElement = PrimeGqElement.fromValue(5, group);

		const exponentGroup: ZqGroup = new ZqGroup(ImmutableBigInteger.fromNumber(3));
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const exponent: ZqElement = ZqElement.create(exponentValue, exponentGroup);

		expect(() => element.exponentiate(exponent)).toThrow(IllegalArgumentError);
	});

	test("Given an exponent with value zero when exponentiate with it then result is one", () => {
		const value: number = 53;
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.ZERO;
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.ONE;

		exponentiateAndAssert(value, exponentValue, expectedResult);
	});

	test("Given element and exponent when exponentiate then succeeds", () => {
		const value: number = 7;
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(49);

		exponentiateAndAssert(value, exponentValue, expectedResult);
	});

	test("Given element and exponent when exponentiation then result smaller than p", () => {
		const value: number =17;
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.fromNumber(5);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(22);

		exponentiateAndAssert(value, exponentValue, expectedResult);
	});

	test("Test exponentiate with a null element", () => {
		const element: PrimeGqElement = PrimeGqElement.fromValue(5, group);

		expect(() => element.exponentiate(null)).toThrow(NullPointerError);
	});

});

test("fromSquareRoot throws an UnsupportedOperationError", () => {
	expect(() => PrimeGqElement.fromSquareRoot()).toThrow(new UnsupportedOperationError('This operation is not supported for PrimeGqElement'));
});


test("Equals method validations", () => {
	const element1_value1_q11: PrimeGqElement = PrimeGqElement.fromValue(5, group);
	const element2_value1_q11: PrimeGqElement = PrimeGqElement.fromValue(5, group);

	const element3_value2_q11: PrimeGqElement = PrimeGqElement.fromValue(17, group);

	const otherGroup_g2_q3: GqGroup = new GqGroup(ImmutableBigInteger.fromNumber(11), ImmutableBigInteger.fromNumber(5), ImmutableBigInteger.fromNumber(3));
	const element4_value1_q3: PrimeGqElement = PrimeGqElement.fromValue(5, otherGroup_g2_q3);

	expect(element1_value1_q11.equals(element2_value1_q11)).toBe(true);
	expect(!element1_value1_q11.equals(element3_value2_q11)).toBe(true);
	expect(!element1_value1_q11.equals(element4_value1_q3)).toBe(true);
	expect(!element3_value2_q11.equals(element4_value1_q3)).toBe(true);
});


/**
 * Multiplies two group elements with the values <code>value1</code> and <code>value2</code>. Then asserts that the result has the value
 * <code>expectedResult</code>.
 *
 * @param value1         First element to multiply.
 * @param value2         Second element to multiply.
 * @param expectedResult The expected result of the <code>value1 * value2</code>.
 */
function multiplyAndAssert(value1: number, value2: number, expectedResult: number) {
	const element1: PrimeGqElement = PrimeGqElement.fromValue(value1, group);
	const element2: PrimeGqElement = PrimeGqElement.fromValue(value2, group);
	const result: GqElement = element1.multiply(element2);
	expect(ImmutableBigInteger.fromNumber(expectedResult).equals(result.value)).toBe(true);
}

/**
 * Exponentiates an element by an exponent and asserts the expected result.
 *
 * @param elementValue   The group element value to set.
 * @param exponentValue  The exponent value to set.
 * @param expectedResult The expected result of the exponentiation.
 */
function exponentiateAndAssert(elementValue: number, exponentValue: ImmutableBigInteger, expectedResult: ImmutableBigInteger) {
	const element: PrimeGqElement = PrimeGqElement.fromValue(elementValue, group);
	const exponent: ZqElement = ZqElement.create(exponentValue, ZqGroup.sameOrderAs(group));
	const result: GqElement = element.exponentiate(exponent);
	expect(expectedResult.equals(result.value)).toBe(true);
}
