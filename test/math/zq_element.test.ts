/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {NullPointerError} from "../../src/error/null_pointer_error";
import {ZqGroup} from "../../src/math/zq_group";
import {ZqElement} from "../../src/math/zq_element";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";


let smallQGroup: ZqGroup;
let largeQGroup: ZqGroup;
let smallQGroupMember: ImmutableBigInteger;
let largeQGroupMember: ImmutableBigInteger;

beforeAll(() => {
	smallQGroup = new ZqGroup(ImmutableBigInteger.fromNumber(11));
	smallQGroupMember = ImmutableBigInteger.fromNumber(2)

	largeQGroup = new ZqGroup(ImmutableBigInteger.fromString(
		"12939396283335421049921068858211433233126495514407886569514225839757682339812461790679331327844644602883220990119774411868903477"
		+
		"19850970560112206096787622837469088478251583519395743196778894805821282742465329909275399786894625491980847224803685372266"
		+
		"94030507127336944889687445102283918380513102809853223420079343830140400246864249360575261041072197363016777415478200207577"
		+
		"30425737985559136062566612003974844221840214834045656737059437540810373459953783841199104522171826073664311433417300419939"
		+
		"05714250940923155511355580701633572104273292197035454235983393288056275740012167103086634201440132309660110514958956970530"
		+ "3"));

	largeQGroupMember = ImmutableBigInteger.fromString(
		"23337993065784550228812110720552652305178266477392633588884900695706615523553977368516877521940228584865573144621632575456086035"
		+
		"44011891370789571610936664154174680840991717947829295213927339653106002172998547312136859057411022087014982249515151970621"
		+
		"03995699012980278133831048916979301493412582679624908502978757946220684184254735784551873442326984628290840105853248774203"
		+
		"43904740081787639502967515631687068869545665294697583750184911025514712871193837246483893950501015755683415509019863976071"
		+
		"64932596862361756821986474438970956308794938908025297141971163638098610004787140454837111247269481459777298855888748030824"
	);
});


describe('Object creation validations', () => {
	test("Given null value when attempt to create exponent then exception", () => {
		expect(() => ZqElement.create(null, smallQGroup)).toThrow(NullPointerError);
	});

	test("Given null group and valid value when attempt to create exponent then exception", () => {
		expect(() => ZqElement.create(ImmutableBigInteger.fromNumber(10), null)).toThrow(NullPointerError);
	});

	test("Given a non random exponent value less than q get exponent value", () => {
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.ONE;
		const exponent: ZqElement = ZqElement.create(exponentValue, smallQGroup);
		expect(exponentValue.equals(exponent.value)).toBe(true);
	});

	test("Given a negative value should throw", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(-1);
		expect(() => ZqElement.create(value, smallQGroup)).toThrow(IllegalArgumentError);
	});

	test("Given a value equal to q should throw", () => {
		const value: ImmutableBigInteger = smallQGroup.q;
		expect(() => ZqElement.create(value, smallQGroup)).toThrow(IllegalArgumentError);
	});

	test("Given a value above q should throw", () => {
		const value: ImmutableBigInteger = smallQGroup.q.add(ImmutableBigInteger.fromNumber(10));
		expect(() => ZqElement.create(value, smallQGroup)).toThrow(IllegalArgumentError);
	});

	test("Given an exponent when get q then expected q returned", () => {
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const expectedQ: ImmutableBigInteger = ImmutableBigInteger.fromNumber(11);
		const exponent: ZqElement = ZqElement.create(exponentValue, smallQGroup);
		expect(expectedQ.equals(exponent.group.q)).toBe(true);
	});

	describe('Constructing a ZqElement with', () => {
		test("a valid int value and a null group throws a NullPointerError.", () => {
			const value: number = 10;
			expect(() => ZqElement.create(value, null)).toThrow(NullPointerError);
		});

		test("a valid int value less than q returns the expected exponent.", () => {
			const value: number = 1;
			const exponentValue: ImmutableBigInteger = ImmutableBigInteger.fromNumber(value);
			const exponent: ZqElement = ZqElement.create(value, smallQGroup);
			expect(exponentValue.equals(exponent.value)).toBe(true);
		});

		test("a negative value throws an IllegalArgumentError.", () => {
			expect(() => ZqElement.create(-1, smallQGroup)).toThrow(IllegalArgumentError);
		});

		test("an int value equal to q throws an IllegalArgumentError.", () => {
			const value: number = smallQGroup.q.intValue();
			expect(() => ZqElement.create(value, smallQGroup)).toThrow(IllegalArgumentError);
		});

		test("an int value above q throws an IllegalArgumentError.", () => {
			const value: number = smallQGroup.q.add(ImmutableBigInteger.fromNumber(10)).intValue();
			expect(() => ZqElement.create(value, smallQGroup)).toThrow(IllegalArgumentError);
		});

		test("a valid int returns the expected q value when getting q.", () => {
			const value: number = 2;
			const expectedQ: ImmutableBigInteger = ImmutableBigInteger.fromNumber(11);
			const exponent: ZqElement = ZqElement.create(value, smallQGroup);
			expect(expectedQ.equals(exponent.group.q)).toBe(true);
		});
	});
});

describe('Add method validations', () => {
	test("Given exponents different groups when add then exception", () => {
		const exponent1: ZqElement = ZqElement.create(smallQGroupMember, smallQGroup);
		const exponent2: ZqElement = ZqElement.create(largeQGroupMember, largeQGroup);
		expect(() => exponent1.add(exponent2)).toThrow(IllegalArgumentError);
	});

	test("Given null exponents when add then exception", () => {
		const exponent1: ZqElement = ZqElement.create(smallQGroupMember, smallQGroup);
		expect(() => exponent1.add(null)).toThrow(NullPointerError);
	});

	test("Given two exponents when added then less than q", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(5);

		addExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});

	test("Given two exponents when added then equals to q", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(5);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(6);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.ZERO;

		addExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});

	test("Given two exponents when added then greater than q", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(10);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.ONE;

		addExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});

	test("Given two equals exponents when added then greater than q", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(10);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(10);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(9);

		addExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});

	test("Given two exponents one equal to zero when added then succeeds", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.ZERO;
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(4);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(4);

		addExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});
});


describe('Negate method validations', () => {
	test("Given an exponent with value zero when negated then result is zero", () => {
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.ZERO;
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.ZERO;

		negateExponentAndAssert(exponentValue, expectedResult);
	});

	test("Given an exponent less than q when negated then succeeds", () => {
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.fromNumber(9);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);

		negateExponentAndAssert(exponentValue, expectedResult);
	});
});


describe('Subtract method validations', () => {
	test("Given two exponents when subtracted result is positive", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.ONE;

		subtractExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});

	test("Given two exponents when subtracted result is zero", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(10);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(10);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.ZERO;

		subtractExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});

	test("Given two exponents when subtracted result is negative", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(10);

		subtractExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});

	test("Given an exponent when subtracted zero then result is the exponent", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(4);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.ZERO;
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(4);

		subtractExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});
});


describe('Multiply method validations', () => {
	test("Given an exponent when multiplied small then result is correct", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(6);

		multiplyExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});

	test("Given an exponent when multiplied big then result is correct", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(6);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.ONE;

		multiplyExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});

	test("Given an exponent when multiplied one then result is equal to exponent", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.ONE;
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);

		multiplyExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});

	test("Given an exponent when multiplied zero then result is zero", () => {
		const exponent1Value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const exponent2Value: ImmutableBigInteger = ImmutableBigInteger.ZERO;
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.ZERO;

		multiplyExponentsAndAssert(exponent1Value, exponent2Value, expectedResult);
	});
});


describe('Exponentiate method validations', () => {
	test("When exponentiated null then throw", () => {
		const element: ZqElement = ZqElement.create(smallQGroupMember, smallQGroup);
		expect(() => element.exponentiate(null)).toThrow(NullPointerError);
	});

	test("When exponentiated negative then throw", () => {
		const element: ZqElement = ZqElement.create(smallQGroupMember, smallQGroup);
		const minusOne: ImmutableBigInteger = ImmutableBigInteger.fromNumber(-1);
		expect(() => element.exponentiate(minusOne)).toThrow(IllegalArgumentError);
	});

	test("When exponentiated zero then result is one", () => {
		exponentiateAndAssert(smallQGroupMember, ImmutableBigInteger.ZERO, ImmutableBigInteger.ONE);
	});

	test("When exponentiated small then result is correct", () => {
		exponentiateAndAssert(smallQGroupMember, ImmutableBigInteger.fromNumber(6), ImmutableBigInteger.fromNumber(9));
	});
});


test("Equals method validation", () => {
	const groupOrder11: ZqGroup = new ZqGroup(ImmutableBigInteger.fromNumber(11));
	const groupOrder12: ZqGroup = new ZqGroup(ImmutableBigInteger.fromNumber(12));
	const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(10);

	const same1: ZqElement = ZqElement.create(value, groupOrder11);
	const same2: ZqElement = ZqElement.create(value, groupOrder11);

	const differentValueSameQ: ZqElement = ZqElement.create(ImmutableBigInteger.fromNumber(7), groupOrder11);
	const sameValueDifferentQ: ZqElement = ZqElement.create(value, groupOrder12);

	expect(same1.equals(same2)).toBe(true);
	expect(same1.equals(differentValueSameQ)).toBe(false);
	expect(differentValueSameQ.equals(sameValueDifferentQ)).toBe(false);
	expect(same1.equals(sameValueDifferentQ)).toBe(false);
});


/**
 * Create the exponents with the given values, and add them. Then assert that the exponent result has the expected value.
 *
 * @param exponent1Value The exponent1 value.
 * @param exponent2Value The exponent2 value.
 * @param expectedResult The expected value of the result of adding those two exponents.
 */
function addExponentsAndAssert(exponent1Value: ImmutableBigInteger, exponent2Value: ImmutableBigInteger, expectedResult: ImmutableBigInteger) {

	let exponent1: ZqElement = ZqElement.create(exponent1Value, smallQGroup);
	const exponent2: ZqElement = ZqElement.create(exponent2Value, smallQGroup);

	exponent1 = exponent1.add(exponent2);

	expect(expectedResult.equals(exponent1.value)).toBe(true);
}

/**
 * Creates the exponent with the <code>exponentValue</code>, and negate it. Asserts that the negated exponent has the <code>expectedValue</code>.
 *
 * @param exponentValue The value for the exponent.
 * @param expectedValue The expected value for the negated exponent.
 */
function negateExponentAndAssert(exponentValue: ImmutableBigInteger, expectedValue: ImmutableBigInteger) {
	const exponent: ZqElement = ZqElement.create(exponentValue, smallQGroup);
	const negated: ZqElement = exponent.negate();

	expect(expectedValue.equals(negated.value)).toBe(true);
}

/**
 * Create the exponents with the given values, and subtract them: <code>(exponent1 - exponent2)</code>. Then assert that the exponent result has the
 * expected value
 *
 * @param exponent1Value The exponent1 value.
 * @param exponent2Value The exponent2 value.
 * @param expectedResult The expected value of the result of subtracting those two exponents: <code>(exponent1 - exponent2)</code>.
 */
function subtractExponentsAndAssert(exponent1Value: ImmutableBigInteger, exponent2Value: ImmutableBigInteger, expectedResult: ImmutableBigInteger) {
	let exponent1: ZqElement = ZqElement.create(exponent1Value, smallQGroup);
	const exponent2: ZqElement = ZqElement.create(exponent2Value, smallQGroup);

	exponent1 = exponent1.subtract(exponent2);

	expect(expectedResult.equals(exponent1.value)).toBe(true);
}

/**
 * Create the exponents with the given values, and multiplies them: {@code (exponent1 * exponent2)}. Then assert that the exponent result has the
 * expected value
 *
 * @param exponent1Value The exponent1 value.
 * @param exponent2Value The exponent2 value.
 * @param expectedResult The expected value of the result of multiplying those two exponents: {@code (exponent1 * exponent2)}.
 */
function multiplyExponentsAndAssert(exponent1Value: ImmutableBigInteger, exponent2Value: ImmutableBigInteger, expectedResult: ImmutableBigInteger) {
	let exponent1: ZqElement = ZqElement.create(exponent1Value, smallQGroup);
	const exponent2: ZqElement = ZqElement.create(exponent2Value, smallQGroup);

	exponent1 = exponent1.multiply(exponent2);

	expect(expectedResult.equals(exponent1.value)).toBe(true);
}

/**
 * Create the {@link ZqElement} with the given {@code elementValue}, and exponentiate it to {@code exponent}. Then assert that the result is the
 * expected value.
 *
 * @param elementValue   The base value.
 * @param exponent       The exponent.
 * @param expectedResult The expected value of the result of exponentiating the base with the exponent: {@code (element ^ exponent)}
 */
function exponentiateAndAssert(elementValue: ImmutableBigInteger, exponent: ImmutableBigInteger, expectedResult: ImmutableBigInteger) {
	const element: ZqElement = ZqElement.create(elementValue, smallQGroup);

	const result: ZqElement = element.exponentiate(exponent);

	expect(expectedResult.equals(result.value)).toBe(true);
}