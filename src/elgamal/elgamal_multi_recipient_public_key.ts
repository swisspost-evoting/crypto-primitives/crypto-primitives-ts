/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GqElement} from "../math/gq_element";
import {GqGroup} from "../math/gq_group";
import {GroupVector} from "../group_vector";
import {checkArgument} from "../validation/preconditions";
import {Hashable} from "src/hashing/hashable";
import {GroupVectorElement} from "../group_vector_element";

/**
 * Encapsulates an ElGamal multi recipient public key with N elements, each corresponding to a different recipient. The order of the elements must
 * match that of the elements of the associated public key.
 *
 * <p>A recipient ElGamal public key is related to its associated ElGamal private key by the following
 * operation: <code>publicKey = g <sup>privateKey</sup> mod p</code>, where g is the generator and p the modulo of the Gq group to which the public
 * key belongs, and privateKey is a member of Zq (notice that Gq and Zq are of the same order). </p>
 *
 * <p>Instances of this class are immutable. </p>
 */
export class ElGamalMultiRecipientPublicKey implements GroupVectorElement<GqGroup> {

	private readonly publicKeyElementsInternal: GroupVector<GqElement, GqGroup>;

	/**
	 * Creates an {@link ElGamalMultiRecipientPublicKey} object.
	 *
	 * @param keyElements <p>the list of public key Gq group publicKeyElements. Must respect the following:
	 *                    <ul>
	 *                    	<li>the list must be non-null.</li>
	 *                    	<li>the list must be non-empty.</li>
	 *                    	<li>the list must contain only non-null elements.</li>
	 *                    	<li>all elements from the list must be from the same mathematical group.</li>
	 *                    </ul>
	 */
	constructor(keyElements: GqElement[]) {
		this.publicKeyElementsInternal = GroupVector.from(keyElements);
		checkArgument(this.publicKeyElementsInternal.elementSize > 0, "An ElGamal public key must not be empty.")
	}

	get group(): GqGroup {
		return this.publicKeyElementsInternal.group;
	}

	get size(): number {
		return this.publicKeyElementsInternal.elements.length;
	}

	get(i: number): GqElement {
		return this.publicKeyElementsInternal.get(i);
	}

	stream(): GqElement[] {
		return this.publicKeyElementsInternal.elements;
	}

	toHashableForm(): Hashable {
		return this.publicKeyElementsInternal.toHashableForm();
	}

	equals(o: ElGamalMultiRecipientPublicKey): boolean {
		if (this === o) {
			return true;
		}
		if (o == null || typeof this !== typeof o) {
			return false;
		}
		return this.publicKeyElementsInternal.equals(o.publicKeyElementsInternal);
	}

}
