/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {SymmetricAuthenticatedEncryptionService} from "./symmetric_authenticated_encryption_service";
import {ImmutableUint8Array} from "../immutable_uint8Array";
import {SymmetricCiphertext} from "./symmetric_ciphertext";
import {SymmetricEncryptionAlgorithm} from "./symmetric_encryption_algorithm";

export class SymmetricService {

	private readonly saeService: SymmetricAuthenticatedEncryptionService;

	public constructor() {
		this.saeService = new SymmetricAuthenticatedEncryptionService(SymmetricEncryptionAlgorithm.AES256_GCM_NOPADDING());
	}

	/**
	 * Symmetric authenticated encryption scheme based on authenticated Encryption with Associated Data (AEAD)
	 *
	 * @param encryptionKey  K ∈ B<sup>k</sup>. Not null.
	 * @param plaintext		 P ∈ B<sup>p</sup>. Not null.
	 * @param associatedData (associated<sub>0</sub>,....,associated<sub>n-1</sub>) ∈ A<sub>UCS</sub><sup>*</sup>)<sup>n</sup>, s.t. n ∈ N. Not null.
	 * @return The authenticated plaintext P ∈ B<sup>p</sup>. Throws an exception if the ciphertext does not authenticate.
	 * @throws IllegalArgumentError if the given encryptionKey is invalid for this underlying algorithm.
	 */
	public async genCiphertextSymmetric(encryptionKey: ImmutableUint8Array, plaintext: ImmutableUint8Array, associatedData: string[]): Promise<SymmetricCiphertext> {
		return this.saeService.genCiphertextSymmetric(encryptionKey, plaintext, associatedData);
	}

	/**
	 * Symmetric authenticated decryption scheme based on authenticated Decryption with Associated Data (AEAD)
	 *
	 * @param encryptionKey  K ∈ B<sup>k</sup>. Not null.
	 * @param ciphertext     C ∈ B<sup>c</sup>. Not null.
	 * @param nonce          nonce ∈ B<sup>n</sup>. Not null.
	 * @param associatedData (associated<sub>0</sub>,....,associated<sub>n-1</sub>) ∈ A<sub>UCS</sub><sup>*</sup>)<sup>n</sup>, s.t. n ∈ N. Not null.
	 * @return plaintextSymmetric - AuthenticatedDecryption(K, nonce, associated, C)
	 * @throws IllegalArgumentError if
	 *                                  <ul>
	 *                                      <li>the given encryptionKey is invalid for this underlying algorithm.</li>
	 *                                      <li>the nonce does not match the expected format.</li>
	 *                                  </ul>
	 */
	public async getPlaintextSymmetric(encryptionKey: ImmutableUint8Array, ciphertext: ImmutableUint8Array, nonce: ImmutableUint8Array,
									   associatedData: string[]): Promise<ImmutableUint8Array> {
		return this.saeService.getPlaintextSymmetric(encryptionKey, ciphertext, nonce, associatedData);
	}
}