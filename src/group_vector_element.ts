/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {MathematicalGroup} from "./math/mathematical_group";
import {Hashable} from "./hashing/hashable";

/**
 * Elements of a GroupVector or GroupMatrix.
 *
 * @param <G> the group type associated.
 */
export interface GroupVectorElement<G extends MathematicalGroup<G>> {
	/**
	 * @return the group this element belongs to.
	 */
	group: G;

	/**
	 * @return the size of this element.
	 */
	size: number;

	toHashableForm(): Hashable;

	equals(o: GroupVectorElement<G>): boolean;
}
