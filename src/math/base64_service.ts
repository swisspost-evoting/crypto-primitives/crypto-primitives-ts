/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {checkArgument, checkNotNull} from "../validation/preconditions";

export class Base64Service {

	private static readonly BASE64_REGEX: RegExp = /^([A-Za-z0-9+\/]{4}){0,149}(([A-Za-z0-9+\/]{4})?|[A-Za-z0-9+\/]{3}=|[A-Za-z0-9+\/]{2}==)$/;
	private static readonly BASE64_MAX_LENGTH: number = 600;

	private static readonly BASE64_ALPHABET = [
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+", "/"
	];

	private static readonly BASE64_CODES = [
		255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, // [0-15]
		255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, // [16-31]
		255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 62, 255, 255, 255, 63, // [32-47] + /
		52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 255, 255, 255, 0, 255, 255, // [48-63] 0 1 2 3 4 5 6 7 8 9 =
		255, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, // [64-79] A B C D E F H G I J K L M N O
		15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 255, 255, 255, 255, 255, // [80-95] P Q R S T U V W X Y Z
		255, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, // [96-111] a b c d e f g h i j k l m n o
		41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51 // [112-122] p q r s t u v w x y z
	];

	/**
	 * Encodes a byte array to a Base64 string.
	 * @param byteArray value to be encoded. Must be non-null.
	 */
	public base64Encode(byteArray: Uint8Array): string {
		checkNotNull(byteArray);
		const B: Uint8Array = Uint8Array.from(byteArray);

		return this.byteArrayToBase64(B);
	}

	/**
	 * Decodes a Base64 string to a byte array.
	 * @param string value to be decoded. Must be non-null, base64 encoded and of max length 600.
	 * @Throws IllegalArgument if input is not base64 encoded or has length greater than 600 chars.
	 */
	public base64Decode(string: string): Uint8Array {
		const S: string = checkNotNull(string);
		checkArgument(this.isBase64(S), "The given string is not a valid Base64 string.");

		return Uint8Array.from(this.base64ToByteArray(S));
	}

	private isBase64(string: string): boolean {
		const l: number = string.length;

		return l <= Base64Service.BASE64_MAX_LENGTH && l % 4 === 0 && Base64Service.BASE64_REGEX.test(string);
	}

	private byteArrayToBase64(byteArray: Uint8Array): string {
		const l: number = byteArray.length;
		let encodedString: string = '',
			i: number;

		for (i = 2; i < l; i += 3) {
			encodedString += Base64Service.BASE64_ALPHABET[byteArray[i - 2] >> 2];
			encodedString += Base64Service.BASE64_ALPHABET[((byteArray[i - 2] & 0x03) << 4) | (byteArray[i - 1] >> 4)];
			encodedString += Base64Service.BASE64_ALPHABET[((byteArray[i - 1] & 0x0F) << 2) | (byteArray[i] >> 6)];
			encodedString += Base64Service.BASE64_ALPHABET[byteArray[i] & 0x3F];
		}
		if (i === l + 1) { // 1 octet yet to write
			encodedString += Base64Service.BASE64_ALPHABET[byteArray[i - 2] >> 2];
			encodedString += Base64Service.BASE64_ALPHABET[(byteArray[i - 2] & 0x03) << 4];
			encodedString += "==";
		}
		if (i === l) { // 2 octets yet to write
			encodedString += Base64Service.BASE64_ALPHABET[byteArray[i - 2] >> 2];
			encodedString += Base64Service.BASE64_ALPHABET[((byteArray[i - 2] & 0x03) << 4) | (byteArray[i - 1] >> 4)];
			encodedString += Base64Service.BASE64_ALPHABET[(byteArray[i - 1] & 0x0F) << 2];
			encodedString += '=';
		}

		return encodedString;
	}

	private base64ToByteArray(string: string) {
		const l: number = string.length,
			decodedByteArray: Uint8Array = new Uint8Array(3 * (l / 4));
		let missingOctets: number = 0;
		missingOctets = string.endsWith('=') ? 1 : missingOctets;
		missingOctets = string.endsWith('==') ? 2 : missingOctets;
		let buffer: number;

		for (let i = 0, j = 0; i < l; i += 4, j += 3) {
			buffer =
				Base64Service.BASE64_CODES[string.charCodeAt(i)] << 18 |
				Base64Service.BASE64_CODES[string.charCodeAt(i + 1)] << 12 |
				Base64Service.BASE64_CODES[string.charCodeAt(i + 2)] << 6 |
				Base64Service.BASE64_CODES[string.charCodeAt(i + 3)];
			decodedByteArray[j] = buffer >> 16;
			decodedByteArray[j + 1] = (buffer >> 8) & 0xFF;
			decodedByteArray[j + 2] = buffer & 0xFF;
		}

		return decodedByteArray.subarray(0, decodedByteArray.length - missingOctets);
	}

}