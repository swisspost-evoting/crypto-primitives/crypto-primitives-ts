/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {checkArgument, checkNotNull} from "../validation/preconditions";

export class Base16Service {

  private static readonly BASE16_REGEX: RegExp = /^([A-F0-9]{2}){0,300}$/;
  private static readonly BASE16_MAX_LENGTH: number = 600;

  /**
   * Encodes a byte array to a Base16 string.
   * @param byteArray value to be encoded. Must be non-null.
   */
  public base16Encode(byteArray: Uint8Array): string {
    checkNotNull(byteArray);
    const B: Uint8Array = Uint8Array.from(byteArray);
    return B.reduce((str, byte) => str + byte.toString(16).padStart(2, '0'), '').toUpperCase();
  }

  /**
   * Decodes a Base16 string to a byte array.
   * @param string value to be decoded. Must be non-null, base16 encoded and of max length 600.
   * @Throws IllegalArgument if input is not base16 encoded or has length greater than 600 chars.
   */
  public base16Decode(string: string): Uint8Array {
    const S: string = checkNotNull(string);
    checkArgument(this.isBase16(S), "The given string is not a valid Base16 string.");

    if (S.length == 0) {
      return Uint8Array.of();
    }
    return Uint8Array.from(S.match(/.{1,2}/g).map((byte) => parseInt(byte, 16)));
  }

  private isBase16(string: string): boolean {
    const l: number = string.length;

    return l <= Base16Service.BASE16_MAX_LENGTH && l % 2 === 0 && Base16Service.BASE16_REGEX.test(string);
  }

}