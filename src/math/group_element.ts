/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {MathematicalGroup} from "./mathematical_group";
import {GroupVectorElement} from "../group_vector_element";
import {ImmutableBigInteger} from "../immutable_big_integer";

/**
 * Representation of a mathematical group element.
 *
 * <p>GroupElements are immutable.
 *
 * @param <G> the type of the mathematical group this group element belongs to.
 */
export abstract class GroupElement<G extends MathematicalGroup<G>> implements GroupVectorElement<G> {

	protected readonly valueInternal: ImmutableBigInteger;
	protected readonly groupInternal: G;

	protected constructor(value: ImmutableBigInteger, group: G) {
		this.valueInternal = value;
		this.groupInternal = group;
	}

	/**
	 * Returns the element value.
	 *
	 * @return element value.
	 */
	get value(): ImmutableBigInteger {
		return this.valueInternal;
	}

	get group(): G {
		return this.groupInternal;
	}

	get size(): number {
		return 1;
	}

	public toHashableForm(): ImmutableBigInteger {
		return this.valueInternal;
	}

	public equals(o: GroupElement<G>): boolean {
		if (this === o) {
			return true;
		}
		if (o == null || typeof this !== typeof o) {
			return false;
		}

		return this.valueInternal.equals(o.valueInternal) && this.group.equals(o.group);
	}

}
