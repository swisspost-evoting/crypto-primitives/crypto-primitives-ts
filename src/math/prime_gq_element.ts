/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {GqElement} from "./gq_element";
import {GqGroup} from "./gq_group";
import {ImmutableBigInteger} from "../immutable_big_integer";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {Primes} from "./primes";
import {UnsupportedOperationError} from "../error/unsupported_operation_error";

// @ts-ignore
// PrimeGqElement's fromValue method needs a number instead of ImmutableBigInteger
export class PrimeGqElement extends GqElement {

	private constructor(value: number, group: GqGroup) {
		super(ImmutableBigInteger.fromNumber(value), group);
	}

	/**
	 * Creates a {@code PrimeGqElement}.
	 * <p>
	 * The {@code value} parameter must be:
	 * <ul>
	 *     <li>non-null.</li>
	 *     <li>an element of the group.</li>
	 *     <li>a small prime.</li>
	 *     <li>different from the group generator.</li>
	 * </ul>
	 *
	 * @param value the value of the element. Must be not-null, a prime element of the group, and different from the group generator.
	 * @param group the {@link GqGroup} to which this element belongs. Must be non-null.
	 * @return a new PrimeGqElement with the specified value in the given group.
	 */
	public static fromValue(value: number, group: GqGroup): PrimeGqElement {
		checkNotNull(value);
		checkNotNull(group);
		checkArgument(Primes.isSmallPrime(value), `Cannot create a PrimeGqElement with given value as it is not a prime element. [value: ${value}]`)
		checkArgument(ImmutableBigInteger.fromNumber(value).compareTo(group.generator.value) !== 0,
			`Cannot create a PrimeGqElement with given value as it is the generator of the group. [value: ${value}, group: ${group}]`);
		checkArgument(group.isGroupMember(ImmutableBigInteger.fromNumber(value)),
			`Cannot create a PrimeGqElement with value ${value} as it is not an element of group ${group}`);
		return new PrimeGqElement(value, group);
	}

	/**
	 * This operation is not supported for PrimeGqElement.
	 * @throws UnsupportedOperationError
	 */
	public static fromSquareRoot(): GqElement {
		throw new UnsupportedOperationError('This operation is not supported for PrimeGqElement');
	}
}
