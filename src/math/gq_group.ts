/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {MathematicalGroup} from "./mathematical_group";
import {GqElement} from "./gq_element";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ImmutableBigInteger} from "../immutable_big_integer";


/**
 * Quadratic residues group of integers modulo p, such that p is a safe prime, i.e. p = 2q + 1. In this case q is the order of the group (ie the
 * number of elements of the group).
 *
 * <p>A group can have multiple generators, which can generate all members of the group through exponentiation.
 *
 * <p>Instances of this class are immutable.
 */
export class GqGroup implements MathematicalGroup<GqGroup> {
	private readonly pInternal: ImmutableBigInteger;
	private readonly qInternal: ImmutableBigInteger;
	private readonly generatorInternal: GqElement;
	private readonly identityInternal: GqElement;

	/**
	 * @param p The modulus.
	 * @param q The order of the group.
	 * @param g A generator of the group.
	 *
	 * <p> Preconditions
	 *             <ul>
	 *           		<li>all arguments are non null</li>
	 * 					<li>p is prime</li>
	 *     				<li>q is prime</li>
	 * 					<li>p = 2q + 1</li>
	 * 					<li>q is in the range [1, p)</li>
	 * 					<li>g is in the range [2, p)</li>
	 * 					<li>g is a member of the group</li>
	 * 				</ul>
	 */
	constructor(p: ImmutableBigInteger, q: ImmutableBigInteger, g: ImmutableBigInteger) {
		checkNotNull(p, "Group Gq parameter p should not be null");
		checkNotNull(q, "Group Gq parameter q should not be null");
		checkNotNull(g, "Group Gq parameter g should not be null");

		// Validate p
		this.pInternal = p;

		// Validate q
		checkArgument(q.compareTo(ImmutableBigInteger.ZERO) > 0);
		checkArgument(q.compareTo(p) < 0);
		const computedP: ImmutableBigInteger = q.multiply(ImmutableBigInteger.fromNumber(2)).add(ImmutableBigInteger.ONE);
		checkArgument(computedP.equals(p), "Group Gq parameter p must be equal to 2q + 1");
		this.qInternal = q;

		// Validate g
		checkArgument(g.compareTo(ImmutableBigInteger.ONE) > 0);
		checkArgument(g.compareTo(p) < 0);
		checkArgument(this.isGroupMember(g), `Group Gq generator g ${g} must be a member of the group`);
		this.generatorInternal = GqElement.fromValue(g, this);

		this.identityInternal = GqElement.fromValue(ImmutableBigInteger.ONE, this);
	}

	get p(): ImmutableBigInteger {
		return this.pInternal;
	}

	get q(): ImmutableBigInteger {
		return this.qInternal;
	}

	get generator(): GqElement {
		return this.generatorInternal;
	}

	get identity(): GqElement {
		return this.identityInternal;
	}

	public isGroupMember(value: ImmutableBigInteger): boolean {
		return value != null && value.compareTo(ImmutableBigInteger.ZERO) > 0 && value.compareTo(this.pInternal) < 0 &&
			value.jacobi(this.pInternal) == 1;
	}

	public equals(o: GqGroup): boolean {
		if (this === o) {
			return true;
		}
		if (o == null || typeof this !== typeof o) {
			return false;
		}

		return this.pInternal.equals(o.pInternal) && this.qInternal.equals(o.qInternal) && this.generatorInternal.value.equals(o.generatorInternal.value);
	}

	public hasSameOrderAs(other: MathematicalGroup<any>): boolean {
		return this.q.equals(other.q);
	}

}