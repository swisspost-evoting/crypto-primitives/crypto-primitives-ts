/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ZqGroup} from "./zq_group";
import {ZqElement} from "./zq_element";
import {GroupVector} from "../group_vector";
import {ImmutableBigInteger} from "../immutable_big_integer";
import {SecureRandomGenerator} from "../generator/secure_random_generator";
import {ImmutableUint8Array} from "../immutable_uint8Array";
import {cutToBitLength} from "../arrays";
import {byteArrayToInteger} from "../conversions";
import {BYTE_SIZE} from "../constants";

export class RandomService {
	private readonly randomInternal: SecureRandomGenerator;

	constructor(random?: SecureRandomGenerator) {
		this.randomInternal = random || new SecureRandomGenerator();
	}

	/**
	 * Generates a random ImmutableBigInteger between 0 (incl.) and <code>upperBound</code> (excl.).
	 *
	 * @param upperBound m, the upper bound. Must be non-null and strictly positive.
	 * @return A random BigInteger <code>r s.t. 0 &le; r &lt; m</code>.
	 */
	public genRandomInteger(upperBound: ImmutableBigInteger): ImmutableBigInteger {
		const m: ImmutableBigInteger = checkNotNull(upperBound);
		checkArgument(upperBound.compareTo(ImmutableBigInteger.ZERO) > 0, "The upper bound must be a positive integer greater than 0.");

		const m_minus_one: ImmutableBigInteger = upperBound.subtract(ImmutableBigInteger.ONE);
		const length: number = this.byteLength(m_minus_one);
		const bitLength: number = m_minus_one.bitLength();
		let r: ImmutableBigInteger;
		do {
			let rBytes: ImmutableUint8Array = cutToBitLength(this.randomBytes(length), bitLength);
			r = byteArrayToInteger(rBytes);
		} while (r.compareTo(m) >= 0);
		return r;
	}

	/**
	 * Computes the length of the byte representation of an integer.
	 *
	 * @param x the integer of which to compute the byte length. Must be non-null.
	 * @return the length of the byte representation of the given integer.
	 * @throws NullPointerError if the given x is null.
	 */
	byteLength(x: ImmutableBigInteger): number {
		checkNotNull(x);

		const n: number = Math.ceil(x.bitLength() / BYTE_SIZE);

		return Math.max(n, 1);
	}

	/**
	 * Generates an array of <code>byteLength</code> random bytes.
	 *
	 * @param byteLength The number of bytes to generate.
	 * @return An array of <code>byteLength</code> random bytes.
	 */
	randomBytes(byteLength: number): ImmutableUint8Array {
		return ImmutableUint8Array.from(this.randomInternal.getBytes(byteLength));
	}

	/**
	 * Generates a vector (collection) of random {@link ZqElement}s between 0 (incl.) and <code>upperBound</code> (excl.).
	 *
	 * @param upperBound q, the exclusive upper bound. Must be non-null and strictly positive.
	 * @param length     n, the desired length. Must be strictly positive.
	 * @return <code>List<ZqElement></code>
	 */
	public genRandomVector(upperBound: ImmutableBigInteger, length: number): GroupVector<ZqElement, ZqGroup> {
		checkNotNull(upperBound);
		checkArgument(upperBound.compareTo(ImmutableBigInteger.ZERO) > 0, "The upper bound must be greater than zero");
		checkArgument(length > 0, "The length must be greater than zero");

		const q: ImmutableBigInteger = upperBound;
		const n: number = length;

		const zqGroup: ZqGroup = new ZqGroup(q);

		const arr: ZqElement[] = [];
		arr.length = n;
		for (let i = 0; i < n; i++) {
			arr[i] = ZqElement.create(this.genRandomInteger(q), zqGroup);
		}
		return GroupVector.from(arr);
	}

	/**
	 * Generates a random number between 0 (incl.) and <code>upperBound</code> (excl.).
	 *
	 * @param upperBound the upper bound. Must be non-null.
	 * @return A random number.
	 */
	public nextInt(upperBound: number): number {
		checkNotNull(upperBound);
		checkArgument(upperBound > 0, "The upper bound must be greater than zero");

		const randomInteger = this.genRandomInteger(ImmutableBigInteger.fromNumber(upperBound));
		return randomInteger.intValue()
	}

}
