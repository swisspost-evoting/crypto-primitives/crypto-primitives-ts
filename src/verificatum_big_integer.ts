/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {sign_t, uli_t, verificatum} from 'vts';
import {checkArgument} from "./validation/preconditions";
import {IllegalArgumentError} from "./error/illegal_argument_error";
import LargeInteger = verificatum.arithm.LI;
import FixModPow = verificatum.arithm.FixModPow;
import ModPowProd = verificatum.arithm.ModPowProd;
import RandomSource = verificatum.base.RandomSource;

export class VerificatumBigInteger {
	static ZERO: VerificatumBigInteger = new VerificatumBigInteger(LargeInteger.ZERO);
	static ONE: VerificatumBigInteger = new VerificatumBigInteger(LargeInteger.ONE);
	static TWO: VerificatumBigInteger = new VerificatumBigInteger(LargeInteger.TWO);
	static TEN: VerificatumBigInteger = new VerificatumBigInteger(Array.of<number>(10));
	public readonly value: LargeInteger;

	/**
	 * Cosntructor 1
	 * @param sign
	 * @param value
	 */
	constructor(sign: number, value: number[]);
	/**
	 *
	 * @param bitlength
	 * @param randomSource
	 */
	constructor(bitlength: number, randomSource: RandomSource); // case 2) length and random source
	constructor(value: number[]); // case 3) byte array of non-negative integer
	constructor(hexString: string); // case 4) hex string
	// constructor() // case 5) byte tree
	constructor(value: number); // case 6) length of 0-filled underlying array
	constructor(value: LargeInteger); // Set the internal LargeInteger value
	constructor(value: string, radix: number) // Parse, either hex or decimal
	constructor(first: number | number[] | string | LargeInteger, second?: number[] | RandomSource | number) {
		if (second != null) {
			this.value = this.getValueFromFirstAndSecond(first, second);
		} else {
			this.value = this.getValueFromFirstOnly(first);
		}
	}

	private getValueFromFirstAndSecond(first: number | number[] | string | verificatum.arithm.LI, second: number[] | verificatum.base.RandomSource | number): LargeInteger {
		if (typeof first === "number") {
			if (Array.isArray(second)) {
				return new LargeInteger(first as sign_t, second as uli_t);
			} else {
				throw new Error("VTS implementation divert from crypto-primitives specification.");
			}
		}
		if (typeof second === "number" && typeof first === "string") {
			if (second === 16) {
				return new LargeInteger(first)
			} else if (second === 10) {
				let parts = first.match(/[^]{1,8}/g);
				let acc = VerificatumBigInteger.ZERO.value;
				for (let part of parts) {
					let factor = new LargeInteger((10 ** part.length).toString(16))
					let current = Number.parseInt(part, 10);
					acc = acc.mul(factor).add(new LargeInteger(current.toString(16)))
				}
				return acc;
			} else {
				throw IllegalArgumentError
			}
		} else {
			throw new Error("Invalid set of arguments");
		}
	}

	private getValueFromFirstOnly(first: number | number[] | string | verificatum.arithm.LI): LargeInteger {
		if (Array.isArray(first)) {
			return new LargeInteger(Array.of<number>(0x00, ...first));
		} else if (typeof first === "string") {
			return new LargeInteger(first);
		} else if (typeof first === "number") {
			// Contrary to LargeInteger's own implementation, we don't need a constructor with a 0-filled array of size `first`
			// Instead, we have a constructor for fixed number values
			return new LargeInteger((first).toString(16));
		} else if (first instanceof LargeInteger) {
			return first;
		}
		return undefined;
	}

	static fixedTable(basis: VerificatumBigInteger, modulus: VerificatumBigInteger, expectedModExps: number): FixModPowTable {
		let fixModPow = new FixModPow(basis.value, modulus.value, expectedModExps);
		return new FixModPowTable(fixModPow);
	}

	static modPowProd(bases: VerificatumBigInteger[], exponents: VerificatumBigInteger[], modulus: VerificatumBigInteger): VerificatumBigInteger {
		let precomp = new ModPowProd(bases.map(b => b.value), modulus.value, 8);
		let result = precomp.modPowProd(exponents.map(e => e.value))
		return new VerificatumBigInteger(result);
	}

	abs(): VerificatumBigInteger {
		return new VerificatumBigInteger(this.value.abs());
	}

	add(other: VerificatumBigInteger): VerificatumBigInteger {
		return new VerificatumBigInteger(this.value.add(other.value))
	}

	bitLength(): number {
		return this.value.bitLength();
	}

	compareTo(other: VerificatumBigInteger): number {
		return this.value.cmp(other.value);
	}

	equals(other: VerificatumBigInteger): boolean {
		return this.value.equals(other.value);
	}

	gcd(other: VerificatumBigInteger): VerificatumBigInteger {
		let egcd: LargeInteger[]
		if (this.value.sign == -1) {
			// VTS gets stuck indefinitely if this.value is negative
			egcd = this.value.mod(other.value).egcd(other.value)
		} else {
			egcd = this.value.egcd(other.value)
		}
		if (egcd.length != 3) {
			throw new Error("Unexpected behaviour from VTS's egcd");
		}
		return new VerificatumBigInteger(egcd[2]);
	}

	intValue(): number {
		return Number.parseInt(this.value.toString());
	}

	isEven(): boolean {
		if (this.value.length > 0) {
			return (this.value.getBit(0) & 1) === 0;
		} else {
			return this.value.sign === 0;
		}
	}

	jacobi(other: VerificatumBigInteger): number {
		return this.value.jacobi(other.value);
	}

	divide(divisor: VerificatumBigInteger) {
		return new VerificatumBigInteger(this.value.divQR(divisor.value)[0])
	}

	mod(modulus: VerificatumBigInteger): VerificatumBigInteger {
		return new VerificatumBigInteger(this.value.mod(modulus.value));
	}

	modAdd(term: VerificatumBigInteger, modulus: VerificatumBigInteger) {
		return new VerificatumBigInteger(this.value.modAdd(term.value, modulus.value));
	}

	modInverse(modulus: VerificatumBigInteger): VerificatumBigInteger {
		checkArgument(modulus.compareTo(VerificatumBigInteger.ZERO) > 0, "modulus must be strictly positive")
		return new VerificatumBigInteger(this.value.modInv(modulus.value));
	}

	modMul(factor: VerificatumBigInteger, modulus: VerificatumBigInteger): VerificatumBigInteger {
		return new VerificatumBigInteger(this.value.modMul(factor.value, modulus.value));
	}

	modPow(exponent: VerificatumBigInteger, modulus: VerificatumBigInteger): VerificatumBigInteger {
		checkArgument(modulus.compareTo(VerificatumBigInteger.ZERO) > 0, "modulus must be strictly positive")
		return new VerificatumBigInteger(this.value.modPow(exponent.value, modulus.value));
	}

	multiply(other: VerificatumBigInteger): VerificatumBigInteger {
		return new VerificatumBigInteger(this.value.mul(other.value));
	}

	negate(): VerificatumBigInteger {
		return new VerificatumBigInteger(this.value.neg());
	}

	testBit(position: number): boolean {
		return this.value.getBit(position) === 1;
	}

	signum(): number {
		return this.value.sign;
	}

	subtract(other: VerificatumBigInteger): VerificatumBigInteger {
		return new VerificatumBigInteger(this.value.sub(other.value));
	}

	toByteArray(): number[] {
		// Contrary to the underlying implementation, we specified that
		// - we only convert positive numbers,
		// - and use unsigned byte array representation instead of two's-complement notation
		checkArgument(this.signum() !== -1, "toByteArray is not specified for negative numbers");
		return VerificatumBigInteger.trimLeading0bytes(this.value.toByteArray());
	}

	private static trimLeading0bytes(array: number[]): number[] {
		for (let i = 0; i < array.length; i++) {
			if (array[i] !== 0) {
				return array.slice(i);
			}
		}
		return [0];
	}

	toString(radix: number = 10): string {
		let stringValue = this.value.toString(radix);
		if (radix === 16) {
			// Normalize hexadecimal representation
			if (stringValue.length % 2 !== 0) {
				stringValue = `0${stringValue}`;
			}
			stringValue = stringValue.toUpperCase();
		}
		return stringValue;
	}

}

export class FixModPowTable {
	private readonly inner: FixModPow;

	constructor(inner: FixModPow) {
		this.inner = inner;
	}

	public modPow(exponent: VerificatumBigInteger): VerificatumBigInteger {
		return new VerificatumBigInteger(this.inner.modPow(exponent.value));
	}
}