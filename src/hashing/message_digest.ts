/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import jsSHA from "jssha";
import {ImmutableUint8Array} from "../immutable_uint8Array";
import {checkNotNull} from "../validation/preconditions";

/**
 * Provides functionality of a message digest algorithm. The SHA3 algorithm is used as the hashing algorithm.
 */
export class MessageDigest {

	private readonly formatInternal = "UINT8ARRAY";
	private readonly algorithmInternal = "SHA3-256";

	constructor() {
		if (!jsSHA) {
			throw new Error(`"jsSHA" is not available`);
		}
	}

	/**
	 * Performs an update on the digest with the data, and then completes the hash computations.
	 *
	 * @param data the data to use for update before the digest is completed.
	 * @return the computed hash as an array of bytes.
	 */
	public digest(data: ImmutableUint8Array): ImmutableUint8Array {
		checkNotNull(data);
		let hash = new jsSHA(this.algorithmInternal, this.formatInternal);

		hash.update(data.value());

		return ImmutableUint8Array.from(hash.getHash(this.formatInternal));
	}

}
