/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {MessageDigest} from "./message_digest";
import {Hashable} from "./hashable";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ImmutableArray} from "../immutable_array";
import {ImmutableUint8Array} from "../immutable_uint8Array";
import {ImmutableBigInteger} from "../immutable_big_integer";
import {concat, cutToBitLength} from "../arrays";
import {byteArrayToInteger, integerToByteArray, stringToByteArray} from "../conversions";
import {IllegalArgumentError} from "../error/illegal_argument_error";
import {ZqElement} from "../math/zq_element";
import {ZqGroup} from "../math/zq_group";
import {ShakeDigest} from "./shake_digest";
import {BYTE_SIZE} from "../constants";
import {GqGroup} from "../math/gq_group";
import {GqElement} from "../math/gq_element";

export class HashService {

    private static readonly HASH_LENGTH_BYTES = 32;
    private static readonly BYTE_ARRAY_PREFIX: ImmutableUint8Array = ImmutableUint8Array.from([0x00]);
    private static readonly BIG_INTEGER_PREFIX: ImmutableUint8Array = ImmutableUint8Array.from([0x01]);
    private static readonly STRING_PREFIX: ImmutableUint8Array = ImmutableUint8Array.from([0x02]);
    private static readonly ARRAY_PREFIX: ImmutableUint8Array = ImmutableUint8Array.from([0x03]);
    private readonly messageDigest: MessageDigest;
    private readonly shakeDigest: ShakeDigest;

    /**
     * Instantiates a recursive hash service with a SHA3-256 message digest and a SHAKE-256 digest.
     */
    public constructor() {
        this.messageDigest = new MessageDigest();
        this.shakeDigest = new ShakeDigest();
    }

    /**
     * Computes the hash of multiple (potentially) recursive inputs.
     *
     * @param values the objects to be hashed.
     * @return the hash of the input.
     *
     * <p> NOTE:
     * <ul>
     *    <li>If the input object(s) are modified during the calculation of the hash, the output is undefined.</li>
     *    <li>It is the caller's responsibility to make sure that the input is not infinite (for example if it contains self-references).</li>
     * </ul>
     */
    public recursiveHash(...values: Hashable[]): ImmutableUint8Array {
        checkNotNull(values);
        checkArgument(values.every(v => v !== null), "Values contain a null value which cannot be hashed.");
        checkArgument(values.length !== 0, "Cannot hash no values.");

        if (values.length > 1) {
            const v: ImmutableArray<Hashable> = ImmutableArray.from(values);
            return this.recursiveHash(v);
        } else {
            const w: Hashable = values[0];

            if (w instanceof ImmutableUint8Array) {
                return this.messageDigest.digest(concat(HashService.BYTE_ARRAY_PREFIX, w));
            } else if (w instanceof ImmutableBigInteger) {
                checkArgument(w.compareTo(ImmutableBigInteger.ZERO) >= 0);
                return this.messageDigest.digest(concat(HashService.BIG_INTEGER_PREFIX, integerToByteArray(w)));
            } else if (typeof w === "string") {
                return this.messageDigest.digest(concat(HashService.STRING_PREFIX, stringToByteArray(w)));
            } else if (w instanceof ImmutableArray) {
                checkArgument(w.length !== 0, "Cannot hash an empty array.");

                const subHashes: ImmutableUint8Array[] = [];
                subHashes.length = w.length;
                for (let j = 0; j < w.length; j++) {
                    subHashes[j] = this.recursiveHash(w.get(j));
                }

                return this.messageDigest.digest(concat(HashService.ARRAY_PREFIX, ...subHashes));
            } else {
                throw new IllegalArgumentError("Object of the given type cannot be hashed.");
            }
        }
    }

    /**
     * Hashes a value and squares the result.
     *
     * @param x the {@link ImmutableBigInteger} to be hashed and squared. Must be non-null.
     * @param group The group to which the returned GqElement has to belong. Must be non-null.
     * @return the squared hash of x as {@link GqElement}
     */
    public hashAndSquare(x: ImmutableBigInteger, group: GqGroup): GqElement {
        checkNotNull(x);
        checkNotNull(group);

        checkArgument(this.getHashLength() * BYTE_SIZE < group.q.bitLength(), "The hash length must be smaller than the bit length of the GqGroup's q.")

        const q: ImmutableBigInteger = group.q;

        const x_h = this.recursiveHashToZq(q, "HashAndSquare", x).value.add(ImmutableBigInteger.ONE);

        return GqElement.fromSquareRoot(x_h, group);
    }

    /**
     * Computes the hash of multiple (potentially) recursive inputs uniformly into Z<sub>q</sub>.
     *
     * @param exclusiveUpperBound q &#8712; &#8469;<sup>+</sup>, the exclusive upper bound for the hash to be returned. Must be strictly positive.
     * @param values              v = (v<sub>0</sub>,...,v<sub>k-1</sub>), the objects to be hashed. Must be non-null.
     * @return the result of the hashing as a {@link ZqElement} smaller than q
     * @throws NullPointerError     if any of the arguments is null
     * @throws IllegalArgumentError if
     *    <ul>
     *        <li>values contain null elements</li>
     *        <li>values are empty</li>
     *        <li>the requested bit length is smaller than 512</li>
     *    </ul>
     */
    public recursiveHashToZq(exclusiveUpperBound: ImmutableBigInteger, ...values: Hashable[]): ZqElement {
        checkNotNull(exclusiveUpperBound);
        checkNotNull(values);
        checkArgument(values.every(value => value !== null), "Values contain a null value which cannot be hashed.");

        const k = values.length;
        const q: ImmutableBigInteger = exclusiveUpperBound;
        const v: Hashable[] = values;

        checkArgument(k > 0, "Cannot hash no values.");
        checkArgument(q.compareTo(ImmutableBigInteger.ZERO) > 0, "The upper bound must be strictly positive.")
        checkArgument(q.bitLength() >= 512, "The exclusive upper bound must have a bit length of at least 512.");

        let h_prime: ImmutableBigInteger = byteArrayToInteger(this.recursiveHashOfLength(q.bitLength() + 256, q, "RecursiveHash", ...v));
        return ZqElement.create(h_prime.mod(q), new ZqGroup(q));
    }

    /**
     * Computes the hash with a requested bit length of multiple (potentially) recursive inputs.
     *
     * @param requestedBitlength the requested bit length of the output hash. Must be strictly greater than 0 with a bit length of at least 512.
     * @param values the objects to be hashed. Non-empty. Must not contain null.
     * @return the hash of the input with the requested bit length
     * @throws NullPointerError if any of the arguments is null
     * @throws IllegalArgumentError if
     * <ul>
     *     <li>the values contain null elements</li>
     *     <li>the requested bit length is smaller than 512</li>
     * </ul>
     */
    public recursiveHashOfLength(requestedBitlength: number, ...values: Hashable[]): ImmutableUint8Array {
        checkNotNull(requestedBitlength);
        checkNotNull(values);
        checkArgument(values.every(v => v !== null), "Values contain a null value which cannot be hashed.");

        const l = requestedBitlength;
        const k = values.length;

        checkArgument(l >= 512, "The requested bit length must be at least 512.");

        const L = Math.ceil(l / BYTE_SIZE);
        if (k > 1) {
            const v: ImmutableArray<Hashable> = ImmutableArray.of(...values);
            return this.recursiveHashOfLength(l, v);
        } else {
            const w = values[0];
            if (w instanceof ImmutableUint8Array) {
                return cutToBitLength(this.shakeDigest.digest(concat(HashService.BYTE_ARRAY_PREFIX, w), L), l);
            } else if (w instanceof ImmutableBigInteger) {
                checkArgument(w.compareTo(ImmutableBigInteger.ZERO) >= 0);
                return cutToBitLength(this.shakeDigest.digest(concat(HashService.BIG_INTEGER_PREFIX, integerToByteArray(w)), L), l);
            } else if (typeof w === "string") {
                return cutToBitLength(this.shakeDigest.digest(concat(HashService.STRING_PREFIX, stringToByteArray(w)), L), l);
            } else if (w instanceof ImmutableArray) {
                checkArgument(w.length !== 0, "Cannot hash an empty array.");

                const j = w.length;
                const subHashes: ImmutableUint8Array[] = [];
                subHashes.length = j;
                for (let i = 0; i < j; i++) {
                    subHashes[i] = this.recursiveHashOfLength(l, w.get(i));
                }

                return cutToBitLength(this.shakeDigest.digest(concat(HashService.ARRAY_PREFIX, ...subHashes), L), l);
            } else {
                throw new IllegalArgumentError();
            }
        }
    }

    /**
     * @return this message digest length in bytes.
     */
    public getHashLength(): number {
        return HashService.HASH_LENGTH_BYTES;
    }
}
