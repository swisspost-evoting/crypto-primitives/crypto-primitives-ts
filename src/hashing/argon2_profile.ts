/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {checkArgument, checkNotNull} from "../validation/preconditions";

/**
 * A collection of predefined {@link Argon2Profile}s.
 * <p>
 *     <ul>
 *         <li>STANDARD - Corresponds to the "uniformly safe option" from RFC-9106</li>
 *         <li>LESS_MEMORY - Corresponds to the "uniformly safe option if less memory available" from RFC-9106</li>
 *         <li>TEST - a profile with reduced memory usage for testing purposes only</li>
 *     </ul>
 * </p>
 */
export class Argon2Profile {

	public static readonly STANDARD = new this(21, 4, 1);
	public static readonly LESS_MEMORY = new this(16, 4, 3);
	public static readonly TEST = new this(14, 4, 1);

	private readonly mInternal: number;
	private readonly pInternal: number;
	private readonly iInternal: number;

	/**
	 * Creates an instance of Argon2Service with the required configuration values in the profile
	 * @param m
	 * @param p
	 * @param i
	 * @throws {@link NullPointerError} if any of the arguments is null.
	 * @throws {@link IllegalArgumentError} if the
	 * 	<ul>
	 * 		<li>memory is not in the expected range of [14, 24]</li>
	 * 		<li>parallelism is not in the expected range of [1, 16]</li>
	 * 		<li>iteration count is not in the expected range of [1, 256]</li>
	 * 	</ul>
	 */
	private constructor(m: number, p: number, i: number) {
		this.mInternal = checkNotNull(m);
		this.pInternal = checkNotNull(p);
		this.iInternal = checkNotNull(i);
		checkArgument(14 <= m && m <= 24, "memory outside of expected range");
		checkArgument(1 <= p && p <= 16, "parallelism outside of expected range");
		checkArgument(1 <= i && i <= 256, "iteration count outside of expected range");
	}

	get m(): number {
		return this.mInternal;
	}

	get	p(): number {
		return this.pInternal;
	}

	get i(): number {
		return this.iInternal;
	}
}
