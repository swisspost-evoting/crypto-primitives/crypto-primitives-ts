/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ExponentiationProofService} from "./exponentiation_proof_service";
import {PlaintextEqualityProofService} from "./plaintext_equality_proof_service";
import {RandomService} from "../math/random_service";
import {HashService} from "../hashing/hash_service";
import {ExponentiationProof} from "./exponentiation_proof";
import {GroupVector} from "../group_vector";
import {GqGroup} from "../math/gq_group";
import {ZqElement} from "../math/zq_element";
import {GqElement} from "../math/gq_element";
import {ElGamalMultiRecipientCiphertext} from "../elgamal/elgamal_multi_recipient_ciphertext";
import {ZqGroup} from "../math/zq_group";
import {PlaintextEqualityProof} from "./plaintext_equality_proof";

export class ZeroKnowledgeProofService {

	private readonly exponentiationProofService: ExponentiationProofService;
	private readonly plaintextEqualityProofService: PlaintextEqualityProofService;

	constructor() {
		const randomService: RandomService = new RandomService();
		const hashService: HashService = new HashService();

		this.exponentiationProofService = new ExponentiationProofService(randomService, hashService);
		this.plaintextEqualityProofService = new PlaintextEqualityProofService(randomService, hashService);
	}

	public genExponentiationProof(bases: GroupVector<GqElement, GqGroup>, exponent: ZqElement, exponentiations: GroupVector<GqElement, GqGroup>,
								  auxiliaryInformation: string[]): ExponentiationProof {
		return this.exponentiationProofService.genExponentiationProof(bases, exponent, exponentiations, auxiliaryInformation);
	}

	public genPlaintextEqualityProof(firstCiphertext: ElGamalMultiRecipientCiphertext, secondCiphertext: ElGamalMultiRecipientCiphertext,
							  firstPublicKey: GqElement, secondPublicKey: GqElement, randomness: GroupVector<ZqElement, ZqGroup>,
							  auxiliaryInformation: string[]): PlaintextEqualityProof {
		return this.plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
			randomness, auxiliaryInformation);
	}

}
