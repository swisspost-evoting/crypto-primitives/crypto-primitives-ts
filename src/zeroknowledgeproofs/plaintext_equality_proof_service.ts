/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GroupVector} from "../group_vector";
import {GqElement} from "../math/gq_element";
import {GqGroup} from "../math/gq_group";
import {ZqGroup} from "../math/zq_group";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {RandomService} from "../math/random_service";
import {HashService} from "../hashing/hash_service";
import {ElGamalMultiRecipientCiphertext} from "../elgamal/elgamal_multi_recipient_ciphertext";
import {byteArrayToInteger} from "../conversions";
import {PlaintextEqualityProof} from "./plaintext_equality_proof";
import {ImmutableArray} from "../immutable_array";
import {Hashable} from "../hashing/hashable";
import {vectorAddition, vectorExponentiation, vectorMultiplication, vectorScalarMultiplication} from "./vector_utils";
import {ImmutableBigInteger} from "../immutable_big_integer";
import {GroupVectorElement} from "../group_vector_element";
import {ZqElement} from "../math/zq_element";


export class PlaintextEqualityProofService {

	private static readonly PLAINTEXT_EQUALITY_PROOF = "PlaintextEqualityProof";

	private randomService: RandomService;
	private hashService: HashService;

	public constructor(randomService: RandomService, hashService: HashService) {
		this.randomService = checkNotNull(randomService);
		this.hashService = checkNotNull(hashService);
	}

	/**
	 * Computes the phi-function for plaintext equality.
	 *
	 * @param preImage        (x, x') ∈ Z<sub>q</sub><sup>2</sup>. Not null.
	 * @param firstPublicKey  h ∈ G<sub>q</sub>. Not null.
	 * @param secondPublicKey h' ∈ G<sub>q</sub>. Not null.
	 * @return an image (g<sup>x</sup>, g<sup>x'</sup>, h<sup>x</sup> / h'<sup>x'</sup>) ∈ G<sub>q</sub><sup>3</sup>
	 * @throws NullPointerError     if any of the parameters is null.
	 * @throws IllegalArgumentError if
	 *                                  <ul>
	 *                                      <li>the preImage is not of size 2</li>
	 *                                      <li>the public keys do not have the same group</li>
	 *                                      <li>the preImage does not have the same group order as the public keys</li>
	 *                                  </ul>
	 */
	static computePhiPlaintextEquality(preImage: GroupVector<ZqElement, ZqGroup>, firstPublicKey: GqElement,
									   secondPublicKey: GqElement): GroupVector<GqElement, GqGroup> {

		checkNotNull(preImage);
		checkNotNull(firstPublicKey);
		checkNotNull(secondPublicKey);
		checkArgument(preImage.size === 2, "The preImage must be of size 2.");

		// Cross group checking.
		checkArgument(firstPublicKey.group.equals(secondPublicKey.group), "The two public keys must have the same group.");
		checkArgument(preImage.group.hasSameOrderAs(firstPublicKey.group), "The preImage and public keys must have the same group order.");

		const g: GqElement = firstPublicKey.group.generator;
		const x: ZqElement = preImage.get(0);
		const x_prime: ZqElement = preImage.get(1);
		const h: GqElement = firstPublicKey;
		const h_prime: GqElement = secondPublicKey;

		return GroupVector.of(g.exponentiate(x), g.exponentiate(x_prime), h.exponentiate(x).divide(h_prime.exponentiate(x_prime)));
	}

	/**
	 * Generates a proof of equality of the plaintext corresponding to the two provided encryptions.
	 *
	 * @param firstCiphertext      C = (c<sub>0</sub>, c<sub>1</sub>) ∈ G<sub>q</sub><sup>2</sup>. Not null.
	 * @param secondCiphertext     C' = (c'<sub>0</sub>, c'<sub>1</sub>) ∈ G<sub>q</sub><sup>2</sup>. Not null.
	 * @param firstPublicKey       h ∈ G<sub>q</sub>. Not null.
	 * @param secondPublicKey      h' ∈ G<sub>q</sub>. Not null.
	 * @param randomness           (r, r') ∈ Z<sub>q</sub><sup>2</sup>. Not null.
	 * @param auxiliaryInformation i<sub>aux</sub>, auxiliary information to be used for the hash. Must be non null. Can be empty.
	 * @return a plaintext equality proof as a {@link PlaintextEqualityProof}.
	 * @throws NullPointerError     if any of the inputs is null or {@code auxiliaryInformation} contains any null.
	 * @throws IllegalArgumentError if
	 *                                  <ul>
	 *                                      <li>the ciphertexts do not contain exactly one phi</li>
	 *                                      <li>the randomness vector does not contain exactly two elements</li>
	 *                                      <li>the ciphertexts and public keys do not belong to the same group</li>
	 *                                      <li>the randomness has a group of different order than the ciphertexts and public keys</li>
	 *                                  </ul>
	 */
	genPlaintextEqualityProof(firstCiphertext: ElGamalMultiRecipientCiphertext, secondCiphertext: ElGamalMultiRecipientCiphertext,
							  firstPublicKey: GqElement, secondPublicKey: GqElement, randomness: GroupVector<ZqElement, ZqGroup>,
							  auxiliaryInformation: string[]): PlaintextEqualityProof {

		checkNotNull(firstCiphertext);
		checkNotNull(secondCiphertext);
		checkNotNull(firstPublicKey);
		checkNotNull(secondPublicKey);
		checkNotNull(randomness);
		checkNotNull(auxiliaryInformation);
		checkArgument(auxiliaryInformation.every(auxInfo => auxInfo != null), "The auxiliary information must not contain null objects.");

		// Dimensions checking.
		checkArgument(firstCiphertext.size === 1, "The first ciphertext must have exactly one phi.");
		checkArgument(secondCiphertext.size === 1, "The second ciphertext must have exactly one phi.");
		checkArgument(randomness.size === 2, "The randomness vector must have exactly two elements.");

		// Cross group checking.
		const gqGroup: GqGroup = firstCiphertext.group;
		const gqGroups: GqGroup[] = [secondCiphertext.group, firstPublicKey.group, secondPublicKey.group];
		checkArgument(gqGroups.every(group => group.equals(gqGroup)), "The ciphertexts and public keys must all belong to the same group.");
		checkArgument(gqGroup.hasSameOrderAs(randomness.group), "The randomness and ciphertexts and public keys must have the same group order.");

		// Context.
		const zqGroup: ZqGroup = randomness.group;
		const p: ImmutableBigInteger = gqGroup.p;
		const q: ImmutableBigInteger = gqGroup.q;
		const g: GqElement = gqGroup.generator;

		// Variables.
		const h: GqElement = firstPublicKey;
		const h_prime: GqElement = secondPublicKey;
		const c_0: GqElement = firstCiphertext.gamma;
		const c_1: GqElement = firstCiphertext.get(0);
		const c_0_prime: GqElement = secondCiphertext.gamma;
		const c_1_prime: GqElement = secondCiphertext.get(0);
		const i_aux: ImmutableArray<string> = ImmutableArray.from(auxiliaryInformation);

		// Operation.
		const b: GroupVector<ZqElement, ZqGroup> = this.randomService.genRandomVector(q, 2);
		const c: GroupVector<GqElement, GqGroup> = PlaintextEqualityProofService.computePhiPlaintextEquality(b, h, h_prime);
		const f: ImmutableArray<Hashable> = ImmutableArray.of(p, q, g.toHashableForm(), h.toHashableForm(), h_prime.toHashableForm());
		const y: GroupVector<GqElement, GqGroup> = GroupVector.of(c_0, c_0_prime, c_1.divide(c_1_prime));

		let h_aux: ImmutableArray<Hashable>;
		if (i_aux.length !== 0) {
			h_aux = ImmutableArray.of<Hashable>(PlaintextEqualityProofService.PLAINTEXT_EQUALITY_PROOF, c_1.toHashableForm(),
				c_1_prime.toHashableForm(), i_aux);
		} else {
			h_aux = ImmutableArray.of<Hashable>(PlaintextEqualityProofService.PLAINTEXT_EQUALITY_PROOF, c_1.toHashableForm(),
				c_1_prime.toHashableForm());
		}

		const eValue: ImmutableBigInteger = byteArrayToInteger(this.hashService.recursiveHash(f, y.toHashableForm(), c.toHashableForm(), h_aux));
		const e: ZqElement = ZqElement.create(eValue, zqGroup);
		const z: GroupVector<ZqElement, ZqGroup> = vectorAddition(b, vectorScalarMultiplication(e, randomness));

		return new PlaintextEqualityProof(e, z);
	}

	/**
	 * Verifies the validity of a plaintext equality proof.
	 *
	 * @param firstCiphertext        C = (c<sub>0</sub>, c<sub>1</sub>) ∈ G<sub>q</sub><sup>2</sup>. Not null.
	 * @param secondCiphertext       C' = (c'<sub>0</sub>, c'<sub>1</sub>) ∈ G<sub>q</sub><sup>2</sup>. Not null.
	 * @param firstPublicKey         h ∈ G<sub>q</sub>. Not null.
	 * @param secondPublicKey        h' ∈ G<sub>q</sub>. Not null.
	 * @param plaintextEqualityProof (e, z), the plaintext equality proof to be verified. Must be non null.
	 * @param auxiliaryInformation   i<sub>aux</sub>, auxiliary information to be used for the hash. Must be non null. Can be empty.
	 * @return {@code true} if the plaintext equality proof is valid, {@code false} otherwise.
	 * @throws NullPointerException     if any of the bases, exponentiations, or plaintextEqualityProof is null
	 * @throws IllegalArgumentError if
	 *                                  <ul>
	 *                                      <li>the ciphertexts do not contain exactly one phi</li>
	 *                                      <li>the ciphertexts and public keys do not belong to the same group</li>
	 *                                      <li>The plaintext equality proof must have the same group order as the ciphertext, the message and the public key.</li>
	 *                                  </ul>
	 */
	verifyPlaintextEquality(firstCiphertext: ElGamalMultiRecipientCiphertext, secondCiphertext: ElGamalMultiRecipientCiphertext,
							firstPublicKey: GqElement, secondPublicKey: GqElement, plaintextEqualityProof: PlaintextEqualityProof,
							auxiliaryInformation: string[]): boolean {

		checkNotNull(firstCiphertext);
		checkNotNull(secondCiphertext);
		checkNotNull(firstPublicKey);
		checkNotNull(secondPublicKey);
		checkNotNull(plaintextEqualityProof);
		checkNotNull(auxiliaryInformation);

		checkArgument(auxiliaryInformation.every(auxInfo => auxInfo != null), "The auxiliary information must not contain null objects.");

		// Dimensions checking.
		checkArgument(firstCiphertext.size === 1, "The first ciphertext must have exactly one phi.");
		checkArgument(secondCiphertext.size === 1, "The second ciphertext must have exactly one phi.");

		// Cross group checking.
		const gqGroups: GroupVectorElement<GqGroup>[] = [firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey];
		checkArgument(gqGroups.every(el => firstCiphertext.group.equals(el.group)), "The ciphertexts and public keys must all belong to the same group.");
		checkArgument(firstCiphertext.group.hasSameOrderAs(plaintextEqualityProof.z.group),
			"The plaintext equality proof must have the same group order as the ciphertexts and the public keys.");

		// Context.
		const gqGroup: GqGroup = firstCiphertext.group;
		const p: ImmutableBigInteger = gqGroup.p;
		const q: ImmutableBigInteger = gqGroup.q;
		const g: GqElement = gqGroup.generator;

		// Variables.
		const c_0: GqElement = firstCiphertext.gamma;
		const c_1: GqElement = firstCiphertext.get(0);
		const c_0_prime: GqElement = secondCiphertext.gamma;
		const c_1_prime: GqElement = secondCiphertext.get(0);
		const h: GqElement = firstPublicKey;
		const h_prime: GqElement = secondPublicKey;
		const ez: PlaintextEqualityProof = plaintextEqualityProof;
		const i_aux: ImmutableArray<string> = ImmutableArray.from(auxiliaryInformation);
		const z: GroupVector<ZqElement, ZqGroup> = ez.z;
		const e: ZqElement = ez.e;

		// Operation.
		const x: GroupVector<GqElement, GqGroup> = PlaintextEqualityProofService.computePhiPlaintextEquality(z, h, h_prime);
		const f: ImmutableArray<Hashable> = ImmutableArray.of(p, q, g.toHashableForm(), h.toHashableForm(), h_prime.toHashableForm());
		const y: GroupVector<GqElement, GqGroup> = GroupVector.of(c_0, c_0_prime, c_1.divide(c_1_prime));
		const y_inverse: GroupVector<GqElement, GqGroup> = GroupVector.from(y.elements.map((y_i) => y_i.invert()));
		const c_prime: GroupVector<GqElement, GqGroup> = vectorMultiplication(x, vectorExponentiation(y_inverse, e));

		let h_aux: ImmutableArray<Hashable>;
		if (i_aux.length !== 0) {
			h_aux = ImmutableArray.of<Hashable>(PlaintextEqualityProofService.PLAINTEXT_EQUALITY_PROOF, c_1.toHashableForm(),
				c_1_prime.toHashableForm(), i_aux);
		} else {
			h_aux = ImmutableArray.of<Hashable>(PlaintextEqualityProofService.PLAINTEXT_EQUALITY_PROOF, c_1.toHashableForm(),
				c_1_prime.toHashableForm());
		}

		const e_prime_value: ImmutableBigInteger = byteArrayToInteger(this.hashService.recursiveHash(f, y.toHashableForm(), c_prime.toHashableForm(), h_aux));
		const e_prime: ZqElement = ZqElement.create(e_prime_value, ZqGroup.sameOrderAs(gqGroup));
		return e.equals(e_prime);
	}
}
