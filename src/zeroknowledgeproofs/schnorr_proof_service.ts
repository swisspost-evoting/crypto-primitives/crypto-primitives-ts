/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ZqElement} from "../math/zq_element";
import {GqGroup} from "../math/gq_group";
import {GqElement} from "../math/gq_element";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ImmutableArray} from "../immutable_array";
import {ZqGroup} from "../math/zq_group";
import {ImmutableBigInteger} from "../immutable_big_integer";
import {RandomService} from "../math/random_service";
import {HashService} from "../hashing/hash_service";
import {Hashable} from "../hashing/hashable";
import {byteArrayToInteger} from "../conversions";
import {ImmutableUint8Array} from "../immutable_uint8Array";
import {SchnorrProof} from "./schnorr_proof";

export class SchnorrProofService {

	private static readonly SCHNORR_PROOF = "SchnorrProof";

	private randomServiceInternal: RandomService;
	private hashServiceInternal: HashService;

	public constructor(randomService: RandomService, hashService: HashService) {
		this.randomServiceInternal = checkNotNull(randomService);
		this.hashServiceInternal = checkNotNull(hashService);
	}

	/**
	 * Computes the phi-function for a Schnorr proof.
	 *
	 * @param exponent x ∈ Z<sub>q</sub>. Not null.
	 * @param base g ∈ G<sub>q</sub>. Not null.
	 * @return y
	 * @throws NullPointerError if any of the parameters is null.
	 */
	static computePhiSchnorr(exponent: ZqElement, base: GqElement): GqElement {
		checkNotNull(exponent);
		checkNotNull(base);

		checkArgument(exponent.group.hasSameOrderAs(base.group), "The exponent must have the same group order as the base.");

		const x: ZqElement = exponent;
		const g: GqElement = base;

		return g.exponentiate(x);
	}

	/**
	 * Generates a proof of knowledge of a discrete logarithm.
	 *
	 * @param witness              x ∈ Z<sub>q</sub>. Not null.
	 * @param statement            y ∈ G<sub>q</sub>. Not null.
	 * @param auxiliaryInformation i<sub>aux</sub>, auxiliary information to be used for the hash. Must be non null. Can be empty.
	 * @return a {@link SchnorrProof}.
	 * @throws NullPointerError     if auxiliary information is null
	 * @throws IllegalArgumentError if the auxiliary information contains null elements
	 */
	genSchnorrProof(witness: ZqElement, statement: GqElement, auxiliaryInformation: string[]): SchnorrProof {

		checkNotNull(witness);
		checkNotNull(statement);
		checkNotNull(auxiliaryInformation);
		checkArgument(auxiliaryInformation.every(auxInfo => auxInfo != null), "The auxiliary information must not contain null objects.");

		// Cross-group checks
		checkArgument(witness.group.hasSameOrderAs(statement.group),
			"The witness must have the same group order as the statement.");
		checkArgument(statement.equals(statement.group.generator.exponentiate(witness)));

		// Context
		const zqGroup: ZqGroup = witness.group;
		const gqGroup: GqGroup = statement.group;
		const p: ImmutableBigInteger = gqGroup.p;
		const q: ImmutableBigInteger = gqGroup.q;
		const g: GqElement = gqGroup.generator;

		// Variables.
		const i_aux = ImmutableArray.from(auxiliaryInformation);
		const y: GqElement = statement;
		const x: ZqElement = witness;

		// Operations.
		const bValue: ImmutableBigInteger = this.randomServiceInternal.genRandomInteger(q);
		const b = ZqElement.create(bValue, zqGroup);
		const c: GqElement = SchnorrProofService.computePhiSchnorr(b, g);
		const f: ImmutableArray<Hashable> = ImmutableArray.of<Hashable>(p, q, g.toHashableForm());

		let h_aux: ImmutableArray<Hashable>;
		if (i_aux.length != 0) {
			h_aux = ImmutableArray.of<Hashable>(SchnorrProofService.SCHNORR_PROOF, i_aux);
		} else {
			h_aux = ImmutableArray.of<Hashable>(SchnorrProofService.SCHNORR_PROOF);
		}

		const eValue = byteArrayToInteger(this.hashServiceInternal.recursiveHash(f, y.toHashableForm(), c.toHashableForm(), h_aux));
		const e = ZqElement.create(eValue, zqGroup);
		const z = b.add(e.multiply(x));

		return new SchnorrProof(e, z);
	}

	/**
	 * Verifies the validity of a Schnorr proof.
	 *
	 * @param proof                (e,z) ∈ Z<sub>q</sub> X Z<sub>q</sub>. Not null.
	 * @param statement            y ∈ G<sub>q</sub>. Not null.
	 * @param auxiliaryInformation i<sub>aux</sub>, auxiliary information to be used for the hash. Must be non null. Can be empty.
	 * @return {@code true} if the schnorr proof is valid, {@code false} otherwise.
	 * @throws NullPointerError if any of the bases, exponentiations, or plaintextEqualityProof is null
	 */
	verifySchnorrProof(proof: SchnorrProof, statement: GqElement, auxiliaryInformation: string[]): boolean {
		checkNotNull(proof);
		checkNotNull(statement);
		checkNotNull(auxiliaryInformation);

		checkArgument(auxiliaryInformation.every(aux_info => aux_info != null), "The auxiliary information must not contain null objects.");

		// Cross-group checking.
		checkArgument(proof.group.hasSameOrderAs(statement.group), "The proof must have the same group order as the statement.");

		// Context.
		const gqGroup: GqGroup = statement.group;
		const p: ImmutableBigInteger = gqGroup.p;
		const q: ImmutableBigInteger = gqGroup.q;
		const g: GqElement = gqGroup.generator;

		// Variables.
		const i_aux: ImmutableArray<string> = ImmutableArray.from(auxiliaryInformation);
		const e: ZqElement = proof.e;
		const z: ZqElement = proof.z;
		const y: GqElement = statement;

		// Operations.
		const x: GqElement = SchnorrProofService.computePhiSchnorr(z, g);
		const f: ImmutableArray<Hashable> = ImmutableArray.of<Hashable>(p, q, g.toHashableForm());

		// Since |e| << |q|, inverting y before exponentiating with e yields better performance than exponentiating y to a negated e
		const c_prime: GqElement = x.multiply(y.invert().exponentiate(e));

		let h_aux: ImmutableArray<Hashable>;
		if (i_aux.length !== 0) {
			h_aux = ImmutableArray.of<Hashable>(SchnorrProofService.SCHNORR_PROOF, i_aux);
		} else {
			h_aux = ImmutableArray.of<Hashable>(SchnorrProofService.SCHNORR_PROOF);
		}

		const h: ImmutableUint8Array = this.hashServiceInternal.recursiveHash(f, y.toHashableForm(), c_prime.toHashableForm(), h_aux);

		const e_prime_value: ImmutableBigInteger = byteArrayToInteger(h);
		const e_prime: ZqElement = ZqElement.create(e_prime_value, ZqGroup.sameOrderAs(gqGroup));
		return e.equals(e_prime);
	}

}
