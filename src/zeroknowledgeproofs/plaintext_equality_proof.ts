/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GroupVector} from "src/group_vector";
import {ZqElement} from "src/math/zq_element";
import {ZqGroup} from "../math/zq_group";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ImmutableArray} from "../immutable_array";
import {Hashable} from "../hashing/hashable";
import {GroupVectorElement} from "../group_vector_element";

export class PlaintextEqualityProof implements GroupVectorElement<ZqGroup> {

	private readonly eInternal: ZqElement;
	private readonly zInternal: GroupVector<ZqElement, ZqGroup>;

	public constructor(e: ZqElement, z: GroupVector<ZqElement, ZqGroup>) {
		checkNotNull(e);
		checkNotNull(z);

		checkArgument(z.size === 2, "z must have exactly two elements.");
		checkArgument(e.group.equals(z.group), "e and z must be from the same group.");

		this.eInternal = e;
		this.zInternal = z;
	}

	get e(): ZqElement {
		return this.eInternal;
	}

	get z(): GroupVector<ZqElement, ZqGroup> {
		return this.zInternal;
	}

	get group(): ZqGroup {
		return this.eInternal.group;
	}

	get size(): number {
		return this.zInternal.size;
	}

	public toHashableForm(): ImmutableArray<Hashable> {
		return ImmutableArray.of<Hashable>(this.eInternal.toHashableForm(), this.zInternal.toHashableForm());
	}

	public equals(o: PlaintextEqualityProof): boolean {
		if (this === o) {
			return true;
		}
		if (o == null || typeof this !== typeof o) {
			return false;
		}
		return this.eInternal.equals(o.eInternal) && this.zInternal.equals(o.zInternal);
	}

}
