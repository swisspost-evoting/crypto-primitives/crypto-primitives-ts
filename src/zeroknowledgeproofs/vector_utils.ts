/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GroupVector} from "../group_vector";
import {ZqElement} from "../math/zq_element";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ZqGroup} from "../math/zq_group";
import {GqElement} from "../math/gq_element";
import {GqGroup} from "../math/gq_group";

/**
 * Adds the first vector to the second one element-wise.
 *
 * @param first  the first vector.
 * @param second the second vector.
 * @return a new {@link GroupVector} which is the result of {@code first} + {@code second}.
 */
export function vectorAddition(first: GroupVector<ZqElement, ZqGroup>, second: GroupVector<ZqElement, ZqGroup>): GroupVector<ZqElement, ZqGroup> {
	checkNotNull(first);
	checkNotNull(second);
	checkArgument(first.size === second.size, "The vectors to be added must have the same size.");
	checkArgument(first.group.equals(second.group), "Both vectors must have the same group.");

	let l: number = first.size;

	let result: ZqElement[] = [];
	result.length = l;

	for (let i = 0; i < l; i++) {
		result[i] = first.get(i).add(second.get(i))
	}

	return GroupVector.from(result);
}

/**
 * Multiply the first vector to the second one element-wise.
 *
 * @param first  the first vector.
 * @param second the second vector.
 * @return a new {@link GroupVector} which is the result of {@code first} * {@code second}.
 */
export function vectorMultiplication(first: GroupVector<GqElement, GqGroup>, second: GroupVector<GqElement, GqGroup>): GroupVector<GqElement, GqGroup> {
	checkNotNull(first);
	checkNotNull(second);
	checkArgument(first.size == second.size, "The vectors to be multiplied must have the same size.");
	checkArgument(first.group.equals(second.group), "Both vectors must have the same group.");

	const l: number = first.size;

	const result: GqElement[] = [];
	result.length = l;

	for (let i = 0; i < l; i++) {
		result[i] = first.get(i).multiply(second.get(i))
	}

	return GroupVector.from(result);
}

/**
 * Exponentiates a vector.
 *
 * @param vector   the vector to exponentiate.
 * @param exponent the value to which to exponentiate the vector.
 * @return a new {@link GroupVector} which is the result of vector<sup>exponent</sup>.
 */
export function vectorExponentiation(vector: GroupVector<GqElement, GqGroup>, exponent: ZqElement): GroupVector<GqElement, GqGroup> {
	checkNotNull(vector);
	checkNotNull(exponent);

	let l: number = vector.size;

	let result: GqElement[] = [];
	result.length = l;

	for (let i = 0; i < l; i++) {
		result[i] = vector.get(i).exponentiate(exponent)
	}

	return GroupVector.from(result);
}

/**
 * Multiplies a vector with a scalar.
 *
 * @param scalar the scalar to be multiplied with.
 * @param vector the vector to be multiplied with.
 * @return the vector resulting from the scalar product {@code scalar} * {@code vector}.
 */
export function vectorScalarMultiplication(scalar: ZqElement, vector: GroupVector<ZqElement, ZqGroup>): GroupVector<ZqElement, ZqGroup> {
	checkNotNull(vector);
	checkNotNull(scalar);
	checkArgument(vector.group.equals(scalar.group), "The scalar must be of the same group than the vector.");

	const l = vector.size;

	let result: ZqElement[] = [];
	result.length = l;
	for (let i = 0; i < l; i++) {
		result[i] = scalar.multiply(vector.get(i));
	}

	return GroupVector.from(result);
}
