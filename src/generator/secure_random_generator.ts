/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as crypto from "crypto";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ImmutableUint8Array} from "../immutable_uint8Array";
import {size_t, verificatum} from "vts";
import RandomSource = verificatum.base.RandomSource;

export class SecureRandomGenerator extends RandomSource {

  /**
   * Provides cryptographically strong random values.
   *
   * This pseudo-random number generation relies on the availability of basic cryptography features available in the current context.
   * It looks for the available crypto module and random method in this order:
   *  - WebAPI Crypto.getRandomValues()
   *  - NodeJS Crypto.randomBytes()
   *  - No available module, it throws an Error
   *
   * @param size The number of random values to generate. Must be non null and strictly positive.
   * @return A TypedArray filled with random values
   */
  public static genRandomBytes(size: number): ImmutableUint8Array {
    checkNotNull(size, "The size can not be null");
    checkArgument(size > 0, "The size must a be a positive number greater than 0.");

    // User agent WebAPI Crypto
    if (typeof window !== "undefined" && window.crypto && window.crypto.getRandomValues) {
      return ImmutableUint8Array.from(window.crypto.getRandomValues(new Uint8Array(size)));
    }

    // Web Workers WebAPI Crypto
    if (typeof self !== "undefined" && self.crypto && self.crypto.getRandomValues) {
      return ImmutableUint8Array.from(self.crypto.getRandomValues(new Uint8Array(size)));
    }

    // NodeJS Crypto module fallback
    if (crypto && crypto.randomBytes) {
      return ImmutableUint8Array.from(crypto.randomBytes(size));
    }

    throw new Error(`Neither "Web API Crypto" nor "NodeJS Crypto" is available`);
  }

  /**
   * Returns bytes from the underlying source of randomness.
   * Verificatum library relies on the implementation of this method as source of randomness.
   *
   * @param len - Number of bytes to generate.
   * @returns Array of random bytes.
   */
  public getRandomUint8Array(len: size_t): Uint8Array {
    return SecureRandomGenerator.genRandomBytes(len).value();
  }
}