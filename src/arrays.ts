/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ImmutableUint8Array} from "./immutable_uint8Array";
import {checkArgument, checkNotNull} from "./validation/preconditions";
import {BYTE_SIZE} from "./constants";

export function concat(...arrays: ImmutableUint8Array[]): ImmutableUint8Array {
	checkArgument(arrays.every(e => e != null), "Arrays must not contain nulls")
	return ImmutableUint8Array.from([].concat(...arrays.map(a => a.toArray())));
}

/**
 * Cuts a byte array to the requested bit length.
 * <p>
 *     Leading zeros are set if the requested length is not a multiple of 8.
 * <\p>
 *
 * @param byteArray B, the byte array to be cut
 * @param requestedBitLength n, the requested bit length. Strictly greater than 0 and smaller than the byte array's bit length.
 * @return a new byte array containing the last n bits of B
 * @throws NullPointerError if any of the arguments is null
 * @throws IllegalArgumentError if the requested bit length is not within the allowed bounds
 */
export function cutToBitLength(byteArray: ImmutableUint8Array, requestedBitLength: number): ImmutableUint8Array {
	checkNotNull(byteArray);
	checkNotNull(requestedBitLength);

	const B = byteArray;
	const n = requestedBitLength;

	const eight: number = BYTE_SIZE;

	checkArgument(0 < n, "The requested length must be strictly positive");
	checkArgument(n <= B.length * eight, "The requested length must not be greater than the bit length of the byte array");

	const length = Math.ceil(n / eight);
	const offset = B.length - length;
	const B_prime = [];
	if (n % eight != 0) {
		B_prime[0] = B.get(offset) & (2 ** (n % eight) - 1);
	} else {
		B_prime[0] = B.get(offset);
	}

	for (let i = 1; i < length; i++) {
		B_prime[i] = B.get(offset + i);
	}
	return ImmutableUint8Array.from(B_prime);
}
